<?php

namespace App;

use App\Models\MasterRecords\UserType;
use App\Models\Reports\GovernmentReport;
use App\Models\Reports\Report;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, EntrustUserTrait;

    /**
     * The table users primary key
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Dates To Be Treated As Carbon Instance
     * @var array
     */
    protected $dates = ['dob'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'middle_name', 'dob', 'gender',
        'email', 'mobile', 'salutation_id', 'lga_id', 'avatar', 'password',
        'user_type_id', 'verification_code', 'verified', 'staus',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'verification_code', 'verified', 'staus'];

    /**
     * Path to the files
     */
    public $file_path = 'pledge_gov/uploads/avatar/';


    public function fullPath(){
        return ($this->avatar) ? env('AMAZON_PATH') . $this->file_path . $this->avatar : null;
    }

    /**
     * Format The Date of Birth Before Inserting
     * @param $date
     */
    public function setDobAttribute($date)
    {
        $this->attributes['dob'] = ($date) ? Carbon::createFromFormat('Y-m-d', $date) : null;
    }

    /**
     * User has many Organisation User
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organisationParentHeads(){
        return $this->hasMany('App\Models\Organisations\OrganisationHead', 'parent_user_id');
    }

    /**
     * User has many Organisation User
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organisationChildHeads(){
        return $this->hasMany('App\Models\Organisations\OrganisationHead', 'child_user_id');
    }

    /**
     * This will get the organisations that the user have
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organisations(){
        return $this->hasMany('App\Models\Organisations\Organisation', 'user_id');
    }

    /**
     * This will get the organisations that the user have
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organisationalHeads(){
        return $this->hasMany('App\Models\Organisations\Organisation', 'head_user_id');
    }

    /**
     * This will get the lga of the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lga(){
        return $this->belongsTo('App\Models\Basic\Lga');
    }

    /**
     * This will get the user type the user belongs to
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userType(){
        return $this->belongsTo('App\Models\MasterRecords\UserType');
    }

    /**
     * This will get the salutation of the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salutation(){
        return $this->belongsTo('App\Models\Basic\Salutation');
    }

    /**
     * Concatenate the first, last and the other names to get full names
     * @return mixed|string
     */
    public function fullNames()
    {
        return ucwords(strtolower($this->first_name . ' ' . $this->last_name . ' ' . $this->middle_name));
    }

    /**
     * Concatenate the first, last and the other names to get full names and also include the salutation
     * @return mixed|string
     */
    public function fullNamesNSalutation()
    {
        $salutation = ($this->salutation_id) ? $this->salutation()->first()->salutation_abbr : '';
        return $salutation . ' ' .ucwords(strtolower($this->first_name . ' ' . $this->last_name . ' ' . $this->middle_name));
    }

    /**
     * Concatenate the first and last names to get full names
     * @return mixed|string
     */
    public function simpleName()
    {
        return ucwords(strtolower($this->first_name . ' ' . $this->last_name));
    }

    /**
     * Concatenate the first and last names to get full names and also include the salutation
     * @return mixed|string
     */
    public function simpleNameNSalutation()
    {
        $salutation = ($this->salutation_id) ? $this->salutation()->first()->salutation_abbr : '';
        return $salutation . ' ' . ucwords(strtolower($this->first_name . ' ' . $this->last_name));
    }

    /**
     * A User belongs to 1 or many Report Sectors
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reportSectors()
    {
        return $this->belongsToMany('App\Models\MasterRecords\Sector', 'report_sectors_users', 'user_id', 'sector_id');
    }

    /**
     * A User belongs to 1 or many Report Sectors
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reportStates()
    {
        return $this->belongsToMany('App\Models\Basic\State', 'report_states_users', 'user_id', 'state_id');
    }

    /**
     * Get the report sectors under this user
     * @param Int $sector_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getReportSectors($sector_id)
    {
        $report = array();
        if(in_array($sector_id, $this->reportSectors()->get()->lists('sector_id')->toArray()) || ($this->reportSectors()->count() === 0)){
            $report = Report::where('sector_id', $sector_id)
                ->whereIn('report_id', GovernmentReport::lists('report_id')->toArray())
                ->where(function ($query) {
                    ($this->reportStates()->count() > 0) ? $query->whereIn('state_id', $this->reportStates()->get()->lists('state_id')->toArray()) : '';
                })
//                ->where(function ($query) {
//                    $query->whereIn('report_id', GovernmentReport::lists('report_id')->toArray());
//                    if(UserType::EXECUTIVES === $this->user_type_id){
//                        $query->whereIn('report_id', GovernmentReport::lists('report_id')->toArray());
//                    }elseif(UserType::LEGISLATORS === $this->user_type_id){
//                        $query->whereIn('report_id', GovernmentReport::lists('report_id')->toArray());
//                    }
//                })
                ->get();
        }
        return $report;
    }

    /**
     * Get the report states under this user
     * @param Int $state_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getReportStates($state_id)
    {
        $report = array();
        if(in_array($state_id, $this->reportStates()->get()->lists('state_id')->toArray())){
            $report = Report::where('state_id', $state_id)
                ->where(function ($query) {
                    ($this->reportSectors()->count() > 0) ? $query->whereIn('sector_id', $this->reportSectors()->get()->lists('sector_id')->toArray()) : '';
                })
                ->where(function ($query) {
                    if(UserType::SECTORS === $this->user_type_id){
                        $query->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::RECENT)->lists('report_id')->toArray());
                    }elseif(UserType::LEGISLATORS === $this->user_type_id){
                        $query->whereIn('report_id', GovernmentReport::where('legislator_status', GovernmentReport::RECENT)->lists('report_id')->toArray());
                    }
                })
                ->get();
        }
        return $report;
    }
}