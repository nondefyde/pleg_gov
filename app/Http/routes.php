<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware' => 'auth'], function() {

    //Dependent List Box
    Route::group(array('prefix'=>'list-box'), function(){
        // Ajax Get Local Governments Based on the state
        Route::get('/lga/{id}', 'ListBoxController@lga');
    });

    Route::get('/', 'DashboardController@getIndex');
    Route::get('/home', 'DashboardController@getIndex');

    Route::controller('users', 'Users\UserController');
    Route::controller('profiles', 'Users\ProfilesController');
    Route::controller('mobile-users', 'Users\MobileUsersController');
    Route::controller('reports', 'Reports\ReportController');

    //Organisations
    Route::group(['middleware' => ['role:organisation_head']], function() {
        Route::controller('organisation-reports', 'Reports\OrganisationReportController');
    });

    Route::group(['middleware' => ['role:sector']], function() {
        Route::controller('organisations', 'Organisations\OrganisationController');
        Route::controller('organisation-heads', 'Organisations\OrganisationHeadsController');
    });
});

Route::controller('auth', 'Auth\AuthController');

//Route::controllers([
//
//    'auth' => 'Auth\AuthController',
//    'users' => 'Users\UserController',
//    'profiles' => 'Users\ProfilesController',
//    'mobile-users' => 'Users\MobileUsersController',
//    'reports' => 'Reports\ReportController',
//
//    //Organisations
//    'organisation-reports' => 'Reports\OrganisationReportController',
//    'organisations' => 'Organisations\OrganisationController',
//    'organisation-heads' => 'Organisations\OrganisationHeadsController',
//]);
