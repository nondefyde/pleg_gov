<?php

namespace App\Http\Controllers;

use App\Models\Basic\Lga;
use App\Http\Requests;

class ListBoxController extends Controller
{
    /**
     * Get the local government areas based on the state id
     * @param int $id
     * @return Response
     */
    public function lga($id)
    {
        $lgas = Lga::where('state_id', $id)->orderBy('lga')->get();

        return view('partials.lga')->with([
            'lgas' => $lgas
        ]);
    }
}
