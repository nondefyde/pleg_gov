<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Reports\OrganisationReport;
use Illuminate\Http\Request;
use App\Models\MasterRecords\UserType;
use App\Models\Reports\GovernmentReport;
use App\Models\MasterRecords\Sector;
use App\Models\Reports\Report;
use Illuminate\Support\Facades\Auth;

class OrganisationReportController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $user = Auth::user();
//        $d = OrganisationReport::whereIn('organisation_id', $user->organisations()->lists('organisation_id')->toArray())->lists('report_id')->toArray();
        $reports = Report::whereIn('report_id', OrganisationReport::whereIn('organisation_id', $user->organisationalHeads()
            ->lists('organisation_id')->toArray())->lists('report_id')->toArray())
            ->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::PENDING)->lists('report_id')->toArray())
            ->orderBy('created_at')->get();

        return view('organisations.reports.index', compact('reports', 'user'));
    }

    /**
     * Approve a Report
     * @param  int  $id
     * @return Response
     */
    public function getApprove($id)
    {
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        if($report) {
            $govt = $report->governmentReport()->first();

            $govt->sector_status = GovernmentReport::APPROVED;

            //update The Government Report Status
            ($govt->save())
                ? $this->setFlashMessage(' Approved!!! '.$report->title.' Report have been sent.', 1)
                : $this->setFlashMessage('Error!!! Unable to perform task try again.', 2);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getApproved()
    {
        $user = Auth::user();
//        $d = OrganisationReport::whereIn('organisation_id', $user->organisations()->lists('organisation_id')->toArray())->lists('report_id')->toArray();
        $reports = Report::whereIn('report_id', OrganisationReport::whereIn('organisation_id', $user->organisationalHeads()
            ->lists('organisation_id')->toArray())->lists('report_id')->toArray())
            ->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::APPROVED)->lists('report_id')->toArray())
            ->orderBy('created_at')->get();

        return view('organisations.reports.approved', compact('reports', 'user'));
    }

    /**
     * Display a listing of the resource.
     * @param String $encodeId
     * @return \Illuminate\Http\Response
     */
    public function getShow($encodeId)
    {
        $user = Auth::user();
        $decodeId = $this->getHashIds()->decode($encodeId);
        $report = (empty($decodeId)) ? abort(305) : Report::findOrFail($decodeId[0]);
        $govt = $report->governmentReport()->first();

        if(UserType::EXECUTIVES === $user->user_type_id){
            $govt->executive_notification = 1;
        }elseif(UserType::LEGISLATORS === $user->user_type_id){
            $govt->legislator_notification = 1;
        }elseif(UserType::SECTORS === $user->user_type_id){
            $govt->sector_notification = 1;
        }
        if($govt){ $govt->save(); }

        return view('organisations.reports.show', compact('report', 'user'));
    }

    /**
     * Display a summary of the report
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getDetail($id)
    {
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        $report->images = $report->images()->get();
        $report->hashedId = $this->getHashIds()->encode($report->report_id);
        $report->path = $report->file_path;
        $report->user = $report->mobileUser()->first();
        $report->state = $report->state()->first()->state;
        $report->user_hashed = $this->getHashIds()->encode($report->mobileUser()->first()->mobile_user_id);
        $report->avatar = ($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg';

        return response()->json($report);
    }
}