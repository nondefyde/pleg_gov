<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\MasterRecords\UserType;
use App\Models\Reports\GovernmentReport;
use App\Models\MasterRecords\Sector;
use App\Models\Reports\Report;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $user = Auth::user();
        $reports = Report::whereIn('report_id', GovernmentReport::lists('report_id')->toArray())
            ->where(function ($query) use ($user) {
                //Check for conditions based(State, Sector) on the roles given to the user
                $this->checkReportCategory($query, $user);
            })
            ->orderBy('created_at')->get();

        return view('reports.index', compact('reports', 'user'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getRecent()
    {
        $user = Auth::user();
        $reports = Report::where(function ($query) use ($user) {
            //(UserType::SECTORS === $user->user_type_id || UserType::EXECUTIVES === $user->user_type_id){
                if(UserType::LEGISLATORS === $user->user_type_id) {
                    $query->whereIn('report_id', GovernmentReport::where('legislator_status', GovernmentReport::RECENT)->lists('report_id')->toArray());
                }else{
                    $query->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::RECENT)->lists('report_id')->toArray());
                }
            })
            ->where(function ($query) use ($user) {
                //Check for conditions based(State, Sector) on the roles given to the user
                $this->checkReportCategory($query, $user);
            })
            ->orderBy('created_at')->get();

        return view('reports.recent', compact('reports', 'user'));
    }

    /**
     * Display a listing of the resource.
     * @param String $encodeId
     * @param String $read
     * @return \Illuminate\Http\Response
     */
    public function getShow($encodeId)
    {
        $user = Auth::user();
        $decodeId = $this->getHashIds()->decode($encodeId);
        $report = (empty($decodeId)) ? abort(305) : Report::findOrFail($decodeId[0]);
        $govt = $report->governmentReport()->first();

        if(UserType::EXECUTIVES === $user->user_type_id){
            $govt->executive_notification = 1;
        }elseif(UserType::LEGISLATORS === $user->user_type_id){
            $govt->legislator_notification = 1;
        }elseif(UserType::SECTORS === $user->user_type_id){
            $govt->sector_notification = 1;
        }
        if($govt){ $govt->save(); }

        return view('reports.show', compact('report', 'user'));
    }

    /**
     * Display a summary of the report
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getDetail($id)
    {
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        $report->images = $report->images();
        $report->hashedId = $this->getHashIds()->encode($report->report_id);
        $report->path = $report->file_path;
        $report->user = $report->mobileUser()->first();
        $report->state = $report->state()->first()->state;
        $report->user_hashed = $this->getHashIds()->encode($report->mobileUser()->first()->mobile_user_id);
        $report->avatar = ($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg';

        return response()->json($report);
    }

    /**
     * Display a modal for report forwarding
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getForwardReport($id)
    {
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        $report->organisations = $report->sector()->first()->organisations()
            ->where('user_id', Auth::user()->user_id)->get(['organisation_id', 'name']);
        $report->state = $report->state()->first()->state;

        return response()->json($report);
    }

    /**
     * Forward the report to the organisations responsible
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postForwardReport(Request $request)
    {
        $user = Auth::user();
        $inputs = $request->all();
        $report = (empty($inputs['report_id'])) ? abort(305) : Report::findOrFail($inputs['report_id']);
        if($report){
            $report->organisations()->sync($inputs['organisation_id']);
            $govt = $report->governmentReport()->first();
            if(UserType::SECTORS === $user->user_type_id){
                $govt->sector_status = GovernmentReport::PENDING;
            }elseif(UserType::LEGISLATORS === $user->user_type_id){
                $govt->legislator_status = GovernmentReport::PENDING;
            }
            //update The Government Report Status
            $govt->save();
        }else{
            $report->organisations()->sync([]);
        }

        // Set the flash message
        $this->setFlashMessage( $report->title . ' report has been forwarded to the organisation(s) responsible', 1);

//        return redirect($request->fullUrl());
        return redirect('/reports');
    }

    /**
     * Forward a Report
     * @param  int  $id
     * @return Response
     */
//    public function getForward($id)
//    {
//        $user = Auth::user();
//        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
//        if($report) {
//            $govt = $report->governmentReport()->first();
//
//            if(UserType::SECTORS === $user->user_type_id){
//                $govt->sector_status = GovernmentReport::PENDING;
//            }elseif(UserType::LEGISLATORS === $user->user_type_id){
//                $govt->legislator_status = GovernmentReport::PENDING;
//            }
//            //update The Government Report Status
//            ($govt->save())
//                ? $this->setFlashMessage(' Forwarded!!! '.$report->title.' Report have been sent.', 1)
//                : $this->setFlashMessage('Error!!! U nable to perform task try again.', 2);
//        }
//    }

    /**
     * Archive the report by disabling the status = 0
     * @param  int  $id
     * @return Response
     */
    public function getArchive($id)
    {
        $user = Auth::user();
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        //Remove The Report Record
        if($report) {
            $govt = $report->governmentReport()->first();

            if(UserType::SECTORS === $user->user_type_id){
                $govt->sector_status = GovernmentReport::ARCHIVED;
            }elseif(UserType::LEGISLATORS === $user->user_type_id){
                $govt->legislator_status = GovernmentReport::ARCHIVED;
            }
            //Update The Government Report for Archiving
            ($govt->save())
                ? $this->setFlashMessage(' Archived!!! '.$report->title.' Report have been archived.', 1)
                : $this->setFlashMessage('Error!!! Unable to Archive record.', 2);
        }
    }

    /**
     * Approve a Report
     * @param  int  $id
     * @return Response
     */
    public function getApprove($id)
    {
        $user = Auth::user();
        $report = (empty($id)) ? abort(305) : Report::findOrFail($id);
        if($report) {
            $govt = $report->governmentReport()->first();

            if(UserType::SECTORS === $user->user_type_id){
                $govt->sector_status = GovernmentReport::APPROVED;
            }elseif(UserType::LEGISLATORS === $user->user_type_id){
                $govt->legislator_status = GovernmentReport::APPROVED;
            }

            //// Extra Logic Here///

            //update The Government Report Status
            ($govt->save())
                ? $this->setFlashMessage(' Approved!!! '.$report->title.' Report have been sent.', 1)
                : $this->setFlashMessage('Error!!! Unable to perform task try again.', 2);
        }
    }

    /**
     * Get all the reports that are from the sector
     * @param String $encodeId
     * @return Response
     */
    public function getSectors($encodeId)
    {
        $user = Auth::user();
        $decodeId = $this->getHashIds()->decode($encodeId);
        $sector = (empty($decodeId)) ? abort(305) : Sector::findOrFail($decodeId[0]);
        $reports = $user->getReportSectors($sector->sector_id);

        return view('reports.sectors', compact('reports', 'user', 'sector'));
    }

    /**
     * Get all pending reports
     * @return Response
     */
    public function getPending()
    {
        $user = Auth::user();
        $reports = Report::where(function ($query) use ($user) {
                if(UserType::LEGISLATORS === $user->user_type_id) {
                    $query->whereIn('report_id', GovernmentReport::where('legislator_status', GovernmentReport::PENDING)->lists('report_id')->toArray());
                }else{
                    $query->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::PENDING)->lists('report_id')->toArray());
                }
            })
            ->where(function ($query) use ($user) {
                //Check for conditions based(State, Sector) on the roles given to the user
                $this->checkReportCategory($query, $user);
            })
            ->orderBy('updated_at', 'desc')->get();

        return view('reports.pending', compact('reports', 'user'));
    }

    /**
     * Get all approved reports
     * @return Response
     */
    public function getApproved()
    {
        $user = Auth::user();
        $reports = Report::where(function ($query) use ($user) {
                if(UserType::LEGISLATORS === $user->user_type_id) {
                    $query->whereIn('report_id', GovernmentReport::where('legislator_status', GovernmentReport::APPROVED)->lists('report_id')->toArray());
                }else{
                    $query->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::APPROVED)->lists('report_id')->toArray());
                }
            })
            ->where(function ($query) use ($user) {
                //Check for conditions based(State, Sector) on the roles given to the user
                $this->checkReportCategory($query, $user);
            })
            ->orderBy('updated_at', 'desc')->get();

        return view('reports.approved', compact('reports', 'user'));
    }

    /**
     * Get all archived reports
     * @return Response
     */
    public function getArchived()
    {
        $user = Auth::user();
        $reports = Report::where(function ($query) use ($user) {
                if(UserType::SECTORS === $user->user_type_id){
                    $query->whereIn('report_id', GovernmentReport::where('sector_status', GovernmentReport::ARCHIVED)->lists('report_id')->toArray());
                }elseif(UserType::LEGISLATORS === $user->user_type_id){
                    $query->whereIn('report_id', GovernmentReport::where('legislator_status', GovernmentReport::ARCHIVED)->lists('report_id')->toArray());
                }
            })
            ->where(function ($query) use ($user) {
                //Check for conditions based(State, Sector) on the roles given to the user
                $this->checkReportCategory($query, $user);
            })
            ->orderBy('updated_at', 'desc')->get();

        return view('reports.archived', compact('reports', 'user'));
    }

    /**
     * Get the newly created report so as to trigger a notification
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getNewReport($id)
    {
        $user = Auth::user();
        $report = (empty($id)) ? abort(305) : Report::find($id);
//            : Report::where('report_id', $id)->where(function ($query) use ($user) {
//                    //Check for conditions based(State, Sector) on the roles given to the user
//                      $this->checkReportCategory($query, $user);
//                });
        if($report) {
            $report->hashId = $this->getHashIds()->encode($report->report_id);
            $report->date = ($report->created_at) ? $report->created_at->format('D, jS, M Y') : '';
            $report->ago = ($report->created_at) ? $report->created_at->diffForHumans() : '';
            $report->names = $report->mobileUser()->first()->fullNames();
            $report->avatar = ($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg';
            return response()->json($report);
        }
    }
}