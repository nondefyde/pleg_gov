<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Khill\Lavacharts\Lavacharts;

class DashboardController extends Controller
{

    public $lava;
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->lava = new Lavacharts;
        $this->middleware('auth');
        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $this->lineChart();
        $this->columnChart();
        $this->areaChart();
        $this->donutChart();
        return view('dashboard.index');
    }

    public function columnChart(){
        $lava = $this->lava;
        $finances = $lava->DataTable();

        $finances->addDateColumn('Year')
            ->addNumberColumn('Power')
            ->addNumberColumn('Health')
            ->addNumberColumn('Education')
            ->addNumberColumn('justice')
            ->addNumberColumn('Commerce')
            ->addNumberColumn('Works')
            ->addNumberColumn('Water')
            ->setDateTimeFormat('Y');

//        $y = 4;
//        for ($a = 1; $a < 30; $a++)
//        {
//            $yr = '200'.$y;
//            $rowData = array(
//                $yr, rand(800,1000), rand(800,1000),rand(800,1000),rand(800,1000),rand(800,1000),rand(800,1000),rand(800,1000)
//            );
//
//            $finances->addRow($rowData);
//            $y++;
//        }

        $finances->addRow(array('2004', 1000, 400,300,354,899,456,455))
            ->addRow(array('2005', 1170, 460,300,354,899,456,455))
            ->addRow(array('2006', 660, 1120,30,345,829,416,455))
            ->addRow(array('2007', 178, 54,300,354,899,45,455))
            ->addRow(array('2008', 1030, 244,306,394,89,562,455))
            ->addRow(array('2009', 130, 54,266,354,899,456,455))
            ->addRow(array('2010', 130, 894,789,354,899,456,455))
            ->addRow(array('2011', 1330, 644,1123,354,99,455,457));

        $columnchart = $lava->ColumnChart('Finances')
            ->setOptions(array(
                'datatable' => $finances,
                'title' => 'Application Performance',
                'titleTextStyle' => $lava->TextStyle(array(
                    'color' => '#eb6b2c',
                    'fontSize' => 14
                ))
            ));

        echo $lava->render('ColumnChart', 'Finances', 'perf_div');
    }


    public function lineChart(){
        $lava = $this->lava;

        $temperatures = $lava->DataTable();

        $temperatures->addDateColumn('Date')
            ->addNumberColumn('Power')
            ->addNumberColumn('Health')
            ->addNumberColumn('Education')
            ->addNumberColumn('justice')
            ->addNumberColumn('Commerce')
            ->addNumberColumn('Works')
            ->addNumberColumn('Water');


        for ($a = 1; $a < 30; $a++)
        {
            $rowData = array(
                "2014-8-$a", rand(800,1000), rand(800,1000),rand(800,1000),rand(800,1000),rand(800,1000),rand(800,1000),rand(800,1000)
            );
            $temperatures->addRow($rowData);
        }

        $linechart = $lava->LineChart('Temps')
            ->dataTable($temperatures)
            ->title('Reports in October');

        echo $lava->render('LineChart', 'Temps', 'line_div');
    }


    public function areaChart(){
        $lava = $this->lava;

        $population = $lava->DataTable();

        $population->addDateColumn('Year')
            ->addNumberColumn('Number of People')
            ->addRow(array('2006', 623452))
            ->addRow(array('2007', 685034))
            ->addRow(array('2008', 716845))
            ->addRow(array('2009', 757254))
            ->addRow(array('2010', 778034))
            ->addRow(array('2011', 792353))
            ->addRow(array('2012', 839657))
            ->addRow(array('2013', 842367))
            ->addRow(array('2014', 873490));

        $areachart = $lava->AreaChart('Population')
            ->setOptions(array(
                'datatable' => $population,
                'title' => 'Population Growth',
                'legend' => $lava->Legend(array(
                    'position' => 'in'
                ))
            ));
        echo $lava->render('AreaChart', 'Population', 'area_div');
    }

    public function donutChart(){
        $lava = $this->lava;
        $reasons = $lava->DataTable();


        $reasons->addStringColumn('Reasons')
            ->addNumberColumn('Percent')
            ->addRow(array('Power', 1895))
            ->addRow(array('Health', 2000))
            ->addRow(array('Education', 1974))
            ->addRow(array('Justice', 1565))
            ->addRow(array('Commerce', 3415))
            ->addRow(array('Works', 5980))
            ->addRow(array('Water', 3146));

        $donutchart = $lava->DonutChart('IMDB')
            ->setOptions(array(
                'datatable' => $reasons,
                'title' => 'Report distribution across sectors'
            ));

        echo $lava->render('DonutChart', 'IMDB', 'donut-chart');
    }
}

