<?php

namespace App\Http\Controllers\Organisations;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Basic\Lga;
use App\Models\Basic\State;
use App\Models\Organisations\Organisation;
use App\Models\Organisations\OrganisationType;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class OrganisationController extends Controller
{
    /**
     * Redirects To The Organisations Default Page
     * @var string
     */
    protected $redirectTo = '/organisations';

    /**
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Get a validator for an incoming registration request.
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
//        $method = $this->getRouter()->getCurrentRequest()->getMethod();
        $messages = [
            'name.required' => 'The Name of Organisation is Required!',
            'sector_id.required' => 'The Organisation\'s Sector is Required!',
            'description.required' => 'The Organisations Registration Code is Required!',
            'email.required' => 'Organisations Valid e-mail Address is Required!',
            'address.required' => 'Organisation\'s contact Address is Required!',
//            'responsibility.required' => 'Responsibility is Required!',
            'logo_url.mimes' => 'Organisation\'s logo must be any of the following formats (jpg,jpeg,png,gif)!',
            'logo_url.max' => 'Organisation\'s logo must be a less than 1MB!',
            'head_user_id.required' => 'Organisation\'s Head is Required!'
        ];
        return Validator::make($data, [
            'sector_id' => 'required',
            'head_user_id' => 'required',
            'name' => 'required',
//            'responsibility' => 'required',
            'description' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'logo_url' => 'mimes:jpg,jpeg,png,gif|max:1024'
        ], $messages);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getIndex()
    {
        $organisations = Auth::user()->organisations()->get();
        return view('organisations.index', compact('organisations'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function getCreate()
    {
        $states = State::orderBy('state')->lists('state', 'state_id')->put('', 'Nothing Selected');
        $organisations = Organisation::orderBy('name')->lists('name', 'organisation_id')->put('', 'Nothing Selected');
        $sectors = Auth::user()->reportSectors()->orderBy('sector')->get(['sector', 'sectors.sector_id']);
        $organisation_heads = Auth::user()->organisationParentHeads()->get();
        return view('organisations.create', compact('states', 'organisations', 'organisation_heads', 'sectors'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function postCreate(Request $request)
    {
        $inputs = $request->all();

        //Validate Request Inputs
        $validator = $this->validator($inputs);

        if ($validator->fails()) {
            $this->setFlashMessage('  Error!!! You have error(s) while filling the form.', 2);
            return redirect('organisations/create')->withErrors($validator)->withInput();
        }
        // Store the Organisation...
        $inputs['user_id'] = Auth::user()->user_id;
        $organisation = Organisation::create($inputs);
        if ($organisation) {
            //upload the pdf files if available
            if ($request->hasFile('logo_url')) {
                $file = $request->file('logo_url');
                $filename = $file->getClientOriginalName();
                $img_ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

                $path = $organisation->file_path . '' . $organisation->organisation_id . '_logo.' . $img_ext;

                $content = File::get($file->getRealPath());
                Flysystem::connection('awss3')->put($path, $content);

                $organisation->logo_url = $organisation->organisation_id . '_logo.' . $img_ext;
                $organisation->save();
            }
            // Set the flash message
            $this->setFlashMessage('  Saved!!! The Organisation has successfully been added.', 1);

            return redirect('organisations/');
        }
    }

    /**
     * Display the specified resource.
     * @param  $encodeId
     * @return Response
     */
    public function getShow($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $organisation = (empty($decodeId)) ? abort(305) : Organisation::findOrFail($decodeId[0]);
        return view('organisations.show', compact('organisation'));
    }

    /**
     * Display a listing of the resource.
     * @param  $encodeId
     * @return Response
     */
    public function getReports($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $organisation = (empty($decodeId)) ? abort(305) : Organisation::findOrFail($decodeId[0]);
        $reports = $organisation->reports()->get();//Organisation::where('user_id', Auth::user()->user_id)->get();
        return view('organisations.reports', compact('reports', 'organisation'));
    }

    /**
     * Show the form for editing a new resource.
     * @param $encodeId
     * @return Response
     */
    public function getEdit($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $organisation = (empty($decodeId)) ? abort(305) : Organisation::findOrFail($decodeId[0]);

        $sectors = Auth::user()->reportSectors()->orderBy('sector')->get(['sector', 'sectors.sector_id']);
        $lga = $organisation->lga()->first();
        $organisation_heads = Auth::user()->organisationHeads()->get();

        $lgas = ($lga !== null) ? Lga::where('state_id', $lga->state_id)->lists('lga', 'lga_id')->put('', 'Nothing Selected') : null;
        $states = State::orderBy('state')->lists('state', 'state_id')->put('', 'Nothing Selected');

        return view('organisations.edit', compact('states', 'sectors', 'organisation', 'lgas', 'lga', 'organisation_heads'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @param  $encodeId
     * @return Response
     */
    public function patchEdit(Request $request, $encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $organisation = (empty($decodeId)) ? abort(305) : Organisation::findOrFail($decodeId[0]);
        $inputs = $request->all();

        //Validate Request Inputs
        $validator = $this->validator($inputs);

        if ($validator->fails()) {
            $this->setFlashMessage('  Error!!! You have error(s) while filling the form.', 2);
            return redirect('organisations/edit/' . $encodeId)->withErrors($validator)->withInput();
        }
        // Store the Organisation...
        $updated = $organisation->update($inputs);
        if ($updated) {
            //upload the pdf files if available
//            if ($request->hasFile('logo_url')) {
//                $file = $request->file('logo_url');
//                $filename = $file->getClientOriginalName();
//                $img_ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
//
//                $path = $organisation->file_path . '' . $organisation->organisation_id . '_logo.' . $img_ext;
//
//                $content = File::get($file->getRealPath());
//                Flysystem::connection('awss3')->put($path, $content);
//
//                $organisation->logo_url = $organisation->organisation_id . '_logo.' . $img_ext;
//                $organisation->save();
//            }
            // Set the flash message
            $this->setFlashMessage('  Saved!!! The Organisation has successfully been updated.', 1);

            return redirect('organisations/');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        $organisation = Organisation::findOrFail($id);
        //Delete The Organisation Record
        $delete = ($organisation !== null) ? $organisation->delete() : null;

        if ($delete) {
            $this->setFlashMessage('Deleted!!! ' . $organisation->name . ' have been removed.', 1);
        } else {
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }
}
