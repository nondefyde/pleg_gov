<?php

namespace App\Http\Controllers\Organisations;

use App\Models\Organisations\OrganisationHead;
use App\Models\RolesAndPermissions\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class OrganisationHeadsController extends Controller
{
    /**
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'first_name.required' => 'The First Name is Required!',
            'last_name.required' => 'The Last Name is Required!',
            'gender.required' => 'Gender is Required!',
            'email.required' => 'Organisation Head e-mail Address is Required!',
            'email.email' => 'A Valid e-mail Address is Required!',
        ];
        return Validator::make($data, [
            'first_name' => 'required|max:100|min:2',
            'last_name' => 'required|max:100|min:2',
            'mobile' => 'min:11',
            'gender' => 'required',
            'email' => 'required|email|max:255|unique:users,email',
        ], $messages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $organisationHeads = Auth::user()->organisationParentHeads()->get();
        return view('organisations.heads.index',compact('organisationHeads'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        return view('organisations.heads.create');
    }

    /**
     * create a new sub user as an organisational head
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request)
    {
        $inputs = $request->all();
        if ($this->validator($inputs)->fails())
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/organisation-heads/create')->withErrors($this->validator($inputs))->withInput();
        }
        //Set the verification code to any random 40 characters and password to random 8 characters
        $verification_code = str_random(40);
        $password = str_random(8);
        $inputs['verification_code'] = $verification_code;
        $inputs['password'] = $password; $temp = '.';
        // Store the User...
        $user = $this->newUser($inputs);
        ///////////////////////////////////////////////////////// mail sending using $user object ///////////////////////////////////////////
//        if($user){
            //Assign a role to the user
//            $role = Role::where('user_type_id', OrganisationHead::USER_TYPE)->first();
//            $user->attachRole($role);

            //Verification Mail Sending
//            $content = 'Welcome to analyzer application, kindly click on the verify link below to complete your registration. Thank You';
//            $content .= "Here are your credentials <br> Username: <strong>" . $user->email . "</strong> <br>";
//            $content .= "Password: <strong>" . $password . "</strong> ";
//            $result = Mail::send('emails.verification', ['user'=>$user, 'content'=>$content], function($message) use($user) {
//                $message->from(env('APP_MAIL'), env('APP_NAME'));
//                $message->subject("Account Verification");
//                $message->to($user->email);
//            });
//            if($result) $temp = ' and a mail has been sent to '.$user->email;
//        }
        if($user){
            OrganisationHead::create(['parent_user_id'=>Auth::user()->user_id, 'child_user_id'=>$user->user_id]);
            // Set the flash message
            $this->setFlashMessage(' Saved!!! Organisation Head '.$user->fullNames().' have successfully been added'.$temp, 1);
            // redirect to the create new warder page
            return redirect('/organisation-heads');
        }
    }

    /**
     * Displays the Organisation Head profiles details
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function getShow($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $user = (empty($decodeId)) ? abort(305) : User::findOrFail($decodeId[0]);
        return view('organisations.heads.show', compact('user'));
    }

    /**
     * delete Organisation Head user and its equivalent sub users record
     * @param  int  $id
     * @return Response
     */
    public function getDelete($id)
    {
        $organisationHeads = OrganisationHead::findOrFail($id);
        //Delete The Organisation Head Record
        $deletedSubUser = ($organisationHeads !== null) ? $organisationHeads->delete() : null;
        //Remove The equivalent Users Record
        if($deletedSubUser) {
            $User = User::findOrFail($organisationHeads->child_user_id);
            $deletedUser = ($User !== null) ? $User->delete() : null;
            ($deletedUser)
                ? $this->setFlashMessage(' Removed!!! Organisation Head '.$User->fullNames().' have been removed.', 1)
                : $this->setFlashMessage('Error!!! Unable to Remove record.', 2);
        }
    }

    /**
     * Enable or Disable a Supervisor Enable : 1, Disable : 0
     * @param  int  $user_id
     * @param  int  $status
     * @return Response
     */
    public function getStatus($user_id, $status)
    {
        $user = User::findOrFail($user_id);
        if($user !== null) {
            $user->status = $status;
            //Update the Supervisor User Status
            if($user->save()){
                ($status === '1')
                    ? $this->setFlashMessage(' Enabled!!! '.$user->fullNames().' Organisation Head have been enabled.', 1)
                    : $this->setFlashMessage(' Disabled!!! '.$user->fullNames().' Organisation Head have been disabled.', 1);
            }else{
                $this->setFlashMessage('Error!!! Unable to perform task try again.', 2);
            }

        }
    }

    /**
     * Create a new user instance after a valid registration.
     * @param  array  $data
     * @return User
     */
    private function newUser(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'mobile' => $data['mobile'],
            'gender' => $data['gender'],
            'user_type_id' => OrganisationHead::USER_TYPE,
            'verification_code' => $data['verification_code']
        ]);
    }
}