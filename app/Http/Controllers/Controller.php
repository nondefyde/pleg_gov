<?php

namespace App\Http\Controllers;

use App\Models\MasterRecords\UserType;
use App\Models\Reports\GovernmentReport;
use App\Models\Reports\Report;
use Hashids\Hashids;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $user = Auth::user();
        if($user) {
            $reports = $this->getUnreadReports($user);
            View::share('report_notifications', $reports);
        }
    }

    /**
     * Set The HashIds Secret Key, Length and Possible Characters Combinations
     * @return Hashids
     */
    public function getHashIds()
    {
        return new Hashids(env('APP_KEY'), 15, env('APP_CHAR'));
    }

    /**
     * @param  string  $msg
     * @param int $type
     * @return void
     */
    public function setFlashMessage($msg, $type)
    {
        $class1 = 'alert-info';
        $class2 = 'fa fa-info fa-2x';

        if($type == 1){
            $class1 = 'alert-success';
            $class2 = 'fa fa-thumbs-o-up fa-2x';
        }elseif($type == 2){
            $class1 = 'alert-danger';
            $class2 = 'fa fa-thumbs-o-down fa-2x';
        }

        $output =   '<div class="alert '.$class1.'" id="flash_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <i class="'.$class2.'"></i> <strong>' . $msg . '</strong>'.
            '</div>';
        \Session::flash('flash_message', $output);
    }

    /**
     * Get all the reports that are un read where read = 0
     * @param User $user
     * @return Response
     */
    private function getUnreadReports($user)
    {
        $reports = Report::where(function ($query) use ($user) {
                if(UserType::EXECUTIVES === $user->user_type_id){
                    $query->whereIn('report_id', GovernmentReport::where('executive_notification', 0)->lists('report_id')->toArray());
                }elseif(UserType::LEGISLATORS === $user->user_type_id){
                    $query->whereIn('report_id', GovernmentReport::where('legislator_notification', 0)->lists('report_id')->toArray());
                }elseif(UserType::SECTORS === $user->user_type_id){
                    $query->whereIn('report_id', GovernmentReport::where('sector_notification', 0)->lists('report_id')->toArray());
                }else{


                    ///Add Code for organisation heads


                    //return [];
                }
            })
            ->where(function ($query) use ($user) {
                //Check for conditions based(State, Sector) on the roles given to the user
                $this->checkReportCategory($query, $user);
            })
            ->orderBy('created_at', 'desc');
//            ->orderBy('created_at', 'desc')->get(['title', 'report_id', 'mobile_user_id', 'created_at']);
        return $reports;
    }

    /**
     * Check for conditions based(State, Sector) on the roles given to the user
     * @param $query
     * @param $user
     */
    public function checkReportCategory($query, $user){
        //Check if any specific state has been assigned
        ($user->reportStates()->count() > 0)
            ? $query->whereIn('state_id', $user->reportStates()->get()->lists('state_id')->toArray()) : '';
        //Check if any specific sector has been assigned
        ($user->reportSectors()->count() > 0)
            ? $query->whereIn('sector_id', $user->reportSectors()->get()->lists('sector_id')->toArray()) : '';
    }
}
