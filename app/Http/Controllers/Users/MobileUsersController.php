<?php

namespace App\Http\Controllers\Users;

use App\Models\MobileUser;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MobileUsersController extends Controller
{
    /**
     * Redirects To The Inmates Default Page
     * @var string
     */
    protected $redirectTo = '/mobile-users';

    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }


    /**
     * Display a listing of the Users.
     * @param String $encodeId
     * @return Response
     */
    public function getActivity($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $mobile_user = (empty($decodeId)) ? abort(305) : MobileUser::findOrFail($decodeId[0]);

        return view('mobile-users.activity', compact('mobile_user'));
    }

    /**
     * Get the mobile user
     * @param Int $id
     * @return Response
     */
    public function getMobileUser($id)
    {
        $mobile_user = (empty($id)) ? abort(305) : MobileUser::findOrFail($id);
        $mobile_user->hashedId = $this->getHashIds()->encode($mobile_user->mobile_user_id);
        $mobile_user->avatar = ($mobile_user->avatar) ? $mobile_user->avatar : '/assets/images/users/no-image.jpg';
        $mobile_user->lga = ($mobile_user->lga_id) ? $mobile_user->lga()->first()->lga : '<label class="label label-danger">nil</label>';
        $mobile_user->state = ($mobile_user->lga_id) ? $mobile_user->lga()->first()->state()->first()->state : '<label class="label label-danger">nil</label>';

        return response()->json($mobile_user);
    }
}
