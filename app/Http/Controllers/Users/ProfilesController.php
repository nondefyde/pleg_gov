<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Basic\Lga;
use App\Models\Basic\Salutation;
use App\Models\Basic\State;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class ProfilesController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'first_name.required' => 'The First Name is Required!',
            'last_name.required' => 'The Last Name is Required!',
            'gender.required' => 'Gender is Required!',
            'salutation_id.required' => 'Salutation is Required!',
            'mobile.required' => 'Phone Number is Required!',
            'dob.required' => 'Date of Birth is Required!',
            'state_id.required' => 'State is Required!',
            'lga_id.required' => 'Local Government Area is Required!',
        ];
        return Validator::make($data, [
            'first_name' => 'required|max:100|min:2',
            'last_name' => 'required|max:100|min:2',
            'salutation_id' => 'required',
            'gender' => 'required',
            'dob' => 'required',
            'mobile' => 'required|min:10',
            'state_id' => 'required',
            'lga_id' => 'required',
        ], $messages);
    }

    /**
     * Displays the user profiles details
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $user = Auth::user();
        $type = 'Profile';
        return view('profiles.index', compact('user', 'type'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getEdit()
    {
        $lga = Auth::user()->lga()->first();
        $states = State::orderBy('state')->lists('state', 'state_id')->put('', 'Nothing Selected');
        $salutations = Salutation::orderBy('salutation')->lists('salutation', 'salutation_id')->put('', 'Nothing Selected');

        $lgas = ($lga !== null) ? Lga::where('state_id', $lga->state_id)->lists('lga', 'lga_id')->put('', 'Nothing Selected') : null;

        return view('profiles.edit', compact('states', 'lga', 'lgas', 'salutations'));
    }

    /**
     * Update the users profile
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(Request $request)
    {
        $inputs = $request->all();
        $user = Auth::user();

        if ($this->validator($inputs)->fails())
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/profiles/edit')->withErrors($this->validator($inputs))->withInput();
        }
        $update = $user->update($inputs);
        if($update) {
            $this->setFlashMessage(' Your profile has been successfully updated.', 1);
            // redirect to the create a new inmate page
            return redirect('/profiles');
        }
    }



    /**
     * Change password of logged in user via profile modal
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChange(Request $request)
    {
        $user = Auth::user();
        $output = array();
        $output['status'] = 0;

        //Validate if the password match the current password
        if (! Hash::check($request->password, $user->password) ) {
            $output['msg'] = 'Warning!!! '.$user->first_name.', Your Old Password Credential did not match your current';
        //validate if the new and confirm password match
        }elseif($request->password_confirmation !== $request->new_password){
            $output['msg'] = 'Warning!!! '.$user->first_name.', Your New and Confirm Password Credential did not match';
        //Store the password...
        }else{
            $user->fill(['password' => Hash::make($request->new_password)])->save();
            $output['status'] = 1;
            $output['msg'] = 'Changed!!! '.$user->username.' Your password change was successful.';
        }
        return Response::json($output);
    }

    /**
     * This will be used to upload profile image of the user
     * @return mixed
     */
    public function postUploadPicture()
    {
        $file = Input::file('file');

        if (!is_null($file)) {

            $filename = $file->getClientOriginalName();
            $img_ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

            $user = Auth::user();
            $user->avatar = $user->file_path . '' . $user->user_id . '_avatar.' . $img_ext;
            Input::file('file')->move($user->file_path, $user->user_id . '_avatar.' . $img_ext);

            $result = $user->save();
            if ($result) {
                return '<div class="cropping-image-wrap"><img src="/'.$user->file_path.$user->avatar.' " class="img-thumbnail" id="crop_image"/></div>';;
            } else {
                return '<div class="alert alert-danger">This format of image is not supported</div>';
            }
        } else {
            return '<div class="alert alert-danger">How did you do that?O_o</div>';
        }
    }

    /**
     *This is used to crop the iage before upload is done
     */
    public function postCropPicture()
    {
        $inputs = Input::all();

        $imgX = $inputs['ic_x'];
        $imgY = $inputs['ic_y'];
        $imgW = $inputs['ic_w'];
        $imgH = $inputs['ic_h'];

        $user = Auth::user();
        $file = File::get($user->avatar);
        $image = Image::make($file);

//        // crop image
        $image =  $image->crop($imgW, $imgH,$imgX,$imgY);
        $result = $image->save($user->avatar,60);

        if($result){
            $file = File::get($user->avatar);
            Flysystem::connection('awss3')->put($user->avatar,$file);
            return $user->avatar . '?'.time();
//            return $user->avatar;
        }
    }
}