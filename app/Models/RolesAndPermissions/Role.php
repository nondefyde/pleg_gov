<?php

namespace App\Models\RolesAndPermissions;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * The table roles primary key
     * @var int
     */
    protected $primaryKey = 'role_id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Get the Pledge Menus associated with the given role
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pledgeMenus()
    {
        return $this->belongsToMany('App\Models\MasterRecords\PledgeMenu', 'pledge_roles_menus', 'role_id', 'pledge_menu_id');
    }

    /**
     * Get the Pledge Menu Items associated with the given role
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pledgeMenuItems()
    {
        return $this->belongsToMany('App\Models\MasterRecords\PledgeMenuItem', 'pledge_roles_menu_items', 'role_id', 'pledge_menu_item_id');
    }

    /**
     * Get the Pledge Sub Menu Items associated with the given role
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pledgeSubMenuItems()
    {
        return $this->belongsToMany('App\Models\MasterRecords\PledgeSubMenuItem', 'pledge_roles_sub_menu_items', 'role_id', 'pledge_sub_menu_item_id');
    }

    /**
     * Get the Menu Items associated with the given role
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'role_user', 'role_id', 'user_id');
    }

}
