<?php

namespace App\Models\Basic;

use Illuminate\Database\Eloquent\Model;

class Lga extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lgas';

    /**
     * The table lgas primary key
     *
     * @var int
     */
    protected $primaryKey = 'lga_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lga', 'lga_code', 'state_id'];


    /**
     * This will get the state of the lga using the belongTo relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state(){
        return $this->belongsTo('App\Models\Basic\State');
    }
}
