<?php

namespace App\Models\Basic;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'states';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'state_id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['state', 'state_code'];


    /**
     * This will get all the lga of the state using the hasMany relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lgas(){
        return $this->hasMany('App\Models\Basic\Lga');
    }

    /**
     * A State belongs to 1 or many Report Users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reportUsers()
    {
        return $this->belongsToMany('App\User', 'report_state_users', 'state_id', 'user_id');
    }
}
