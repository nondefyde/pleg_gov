<?php

namespace App\Models\Basic;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * The table constituencies primary key
     *
     * @var int
     */
    protected $primaryKey = 'status_id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'status';

    /**
     * disable the time stamps
     * @var bool
     */
    public $timestamps = false;
}
