<?php

namespace App\Models\Organisations;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organisations';

    /**
     * Path to the files
     */
    public $file_path = 'uploads/organisations/';


    /**
     * The table user_details primary key
     *
     * @var int
     */
    protected $primaryKey = 'organisation_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'cac_registration_no', 'address',
                            'email','phone_no','description','logo_url', 'responsibility',
                            'sector_id', 'lga_id', 'head_user_id', 'user_id'];

    /**
     * This will get the lga of the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lga(){
        return $this->belongsTo('App\Models\Basic\Lga');
    }


    /**
     * This will get the full path to the image
     * @return String
     */
    public function fullUploadPath(){
        return env('AMAZON_PATH').''.$this->file_path.''.$this->logo_url;
    }

    /**
     * This will get the sector the organization belongs to
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sector(){
        return $this->belongsTo('App\Models\MasterRecords\Sector', 'sector_id');
    }

    /**
     * This will get the main user that created this organisation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }


    /**
     * This will get the organisation child user that heads the organisation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organisationHeadUser(){
        return $this->belongsTo('App\User', 'head_user_id');
    }

    /**
     * Get the reports associated with the given organisation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reports()
    {
        return $this->belongsToMany('App\Models\Reports\Report', 'organisation_reports', 'organisation_id', 'report_id');
    }
}
