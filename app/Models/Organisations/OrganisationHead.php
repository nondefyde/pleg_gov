<?php

namespace App\Models\Organisations;

use Illuminate\Database\Eloquent\Model;

class OrganisationHead extends Model
{
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'organisation_heads';

    /**
     * The table users primary key
     *
     * @var string
     */
    protected $primaryKey = 'organisation_head_id';

    /**
     * Set the User Type of a User to 10
     */
    const USER_TYPE = 10;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['parent_user_id', 'child_user_id'];

    /**
     * This will get the organisation parent user that created the organisation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentUser(){
        return $this->belongsTo('App\User', 'parent_user_id');
    }

    /**
     * This will get the organisation child user that heads the organisation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function childUser(){
        return $this->belongsTo('App\User', 'child_user_id');
    }
}
