<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobileUser extends Model
{
    /**
     * The table users primary key
     * @var string
     */
    protected $primaryKey = 'mobile_user_id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mobile_users';

    /**
     * This will get the lga of the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lga(){
        return $this->belongsTo('App\Models\Basic\Lga');
    }

    /**
     * Concatenate the first, last and the other names to get full names
     * @return mixed|string
     */
    public function fullNames()
    {
        return ucwords(strtolower($this->first_name . ' ' . $this->last_name));
    }

    /**
     * Mobile User has many reports
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports(){
        return $this->hasMany('App\Models\Reports\Report');
    }
}
