<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'locations';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'location_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country', 'state', 'lga_city', 'address'];

    /**
     * disable the time stamps
     * @var bool
     */
    public $timestamps = false;
}