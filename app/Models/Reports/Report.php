<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reports';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'report_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'images', 'report_time', 'sector_id', 'mobile_user_id', 'state_id', 'location_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['forward', 'archive', 'notification'];

    /**
     * Dates To Be Treated As Carbon Instance
     * @var array
     */
    protected $dates = ['report_time'];

    /**
    * Path to the files
    */
    public $file_path = '/uploads/reports/';

    /**
     * This will get the state of the report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state(){
        return $this->belongsTo('App\Models\Basic\State');
    }

    /**
     * This will get the mobile user that made the report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mobileUser(){
        return $this->belongsTo('App\Models\MobileUser');
    }

    /**
     * This will get the sector of the report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sector(){
        return $this->belongsTo('App\Models\MasterRecords\Sector');
    }

    /**
     * Report has many Report Images
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    public function images(){
//        return $this->hasMany('App\Models\Reports\ReportImage');
//    }

    /**
     * Reports Images Spilt it
     * @return array
     */
    public function images(){
        if($this->images === null){
            return [];
        }else{
            return explode('|', $this->images);
        }
    }

    /**
     * This will get the government report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function governmentReport(){
        return $this->hasOne('App\Models\Reports\GovernmentReport');
    }

    /**
     * This will get the location of the report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location(){
        return $this->belongsTo('App\Models\Reports\Location');
    }

    /**
     * Get the organisations associated with the given report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organisations()
    {
        return $this->belongsToMany('App\Models\Organisations\Organisation', 'organisation_reports', 'report_id', 'organisation_id');
    }
}