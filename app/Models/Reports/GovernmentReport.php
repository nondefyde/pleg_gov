<?php

namespace App\Models\Reports;

use App\Models\MasterRecords\UserType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GovernmentReport extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'government_reports';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'government_report_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['report_id'];

    const APPROVED = 1;
    const PENDING = 2;
    const ARCHIVED = 3;
    const RECENT = 0;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['sector_status', 'legislator_status', 'sector_notification', 'executive_notification', 'legislator_notification'];

    /**
     * This will get the report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report(){
        return $this->belongsTo('App\Models\Reports\Report');
    }

    /**
     * This will get the report status on at the sectors level
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sectorStatus(){
        return $this->belongsTo('App\Models\Basic\Status', 'sector_status', 'status_id');
    }

    /**
     * This will get the report status on at the legislators level
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function legislatorStatus(){
        return $this->belongsTo('App\Models\Basic\Status', 'legislator_status', 'status_id');
    }

    /**
     * Get the reports based on the type of user
     * @return \App\Models\Basic\Status
     */
    public function getStatus(){
        $user = Auth::user();
        if(UserType::EXECUTIVES === $user->user_type_id){
            return $this->sectorStatus();
        }elseif(UserType::LEGISLATORS === $user->user_type_id){
            return $this->legislatorStatus();
        }elseif(UserType::SECTORS === $user->user_type_id){
            return $this->sectorStatus();
        }
    }
}