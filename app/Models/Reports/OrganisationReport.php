<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class OrganisationReport extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organisation_reports';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'organisation_report_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['organisation_id', 'report_id'];

    /**
     * disable the time stamps
     * @var bool
     */
    public $timestamps = false;
}