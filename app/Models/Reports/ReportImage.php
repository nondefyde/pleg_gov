<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reports_images';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'report_image_id';


    /**
     * Path to the files
     */
    public $file_path = '/uploads/reports/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['report_id', 'image_url'];

    /**
     * This will get the report that belongs to the report image
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report(){
        return $this->belongsTo('App\Models\Reports\Report');
    }
}