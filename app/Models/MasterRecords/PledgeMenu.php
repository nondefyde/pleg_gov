<?php

namespace App\Models\MasterRecords;

use Illuminate\Database\Eloquent\Model;

class PledgeMenu extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pledge_menus';

    /**
     * The table Menus primary key
     *
     * @var int
     */
    protected $primaryKey = 'pledge_menu_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pledge_menu', 'pledge_menu_url', 'pledge_icon', 'active', 'sequence'];

    /**
     * A Menu has many Menu Items
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function pledgeMenuItems(){
        return $this->hasMany('App\Models\MasterRecords\PledgeMenuItem');
    }

    /**
     * Get the roles associated with the given menu
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pledgeRoles()
    {
//        return $this->belongsToMany('App\Models\RolesAndPermissions\Role', 'roles_menus', 'menu_id', 'role_id')->withPivot('menu_item_id');
        return $this->belongsToMany('App\Models\RolesAndPermissions\Role', 'pledge_roles_menus', 'pledge_menu_id', 'role_id');
    }
}
