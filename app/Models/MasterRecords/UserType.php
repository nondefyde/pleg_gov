<?php

namespace App\Models\MasterRecords;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_types';


    /**
     * The table user_details primary key
     *
     * @var int
     */
    protected $primaryKey = 'user_type_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_type'];

    const OWNER = 1;
    const DEVELOPER = 2;
    const USER = 3;
    const EXECUTIVES = 4;
    const LEGISLATORS = 5;
    const SECTORS = 6;

    public $PLEDGE = [1,2,3];
    public $PLEDGE_GOVT = [4,5,6,10];
}
