<?php

namespace App\Models\MasterRecords;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sectors';

    /**
     * The table states primary key
     *
     * @var int
     */
    protected $primaryKey = 'sector_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sector'];

    /**
     * Federal Sectors
     */
    const FEDERAL_SECTORS = 0;

    /**
     * State Sectors
     */
    const STATE_SECTORS = 1;

    /**
     * A Sector belongs to 1 or many Report Users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reportUsers()
    {
        return $this->belongsToMany('App\User', 'report_sector_users', 'sector_id', 'user_id');
    }

    /**
     * Sector has many Organisations
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organisations(){
        return $this->hasMany('App\Models\Organisations\Organisation');
    }
}
