<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>:: {{ env('APP_NAME') }} | @yield('title') ::</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    @yield('before_custom_css')
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->


    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('/css/theme-forest.css') }}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('css/style.css') }}"/>

    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    @yield('custom_css')
            <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <!-- EOF CSS INCLUDE -->
</head>
<body onload="javascript:showTime();">
<!-- START PAGE CONTAINER -->
<div class="page-container">

    @include('layouts.sidebar')

            <!-- PAGE CONTENT -->
    <div class="page-content">
        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- TOGGLE NAVIGATION -->
            <li class="xn-icon-button">
                <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
            </li>
            <!-- END TOGGLE NAVIGATION -->
            <!-- SEARCH -->
            <li class="xn-search">
                <form role="form">
                    <input type="text" name="search" placeholder="Search..."/>
                </form>
            </li>
            <!-- END SEARCH -->
            <!-- POWER OFF -->
            <li class="xn-icon-button pull-right last">
                <a href="#"><span class="fa fa-power-off"></span></a>
                <ul class="xn-drop-left animated zoomIn">
                    <li><a href="/users/change"><span class="fa fa-lock"></span> Password Change</a></li>
                    <li><a href="/auth/logout"><span class="fa fa-power-off text-danger"></span> <span class="xn-text"> Logout</span></a></li>
                </ul>
            </li>
            <!-- END POWER OFF -->
            <!-- MESSAGES -->
            {{--<li class="xn-icon-button pull-right">--}}
                {{--<a href="#"><span class="fa fa-comments"></span></a>--}}
                {{--<div class="informer informer-danger">4</div>--}}
                {{--<div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>--}}
                        {{--<div class="pull-right">--}}
                            {{--<span class="label label-danger">4 new</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">--}}
                        {{--<a href="#" class="list-group-item">--}}
                        {{--<div class="list-group-status status-online"></div>--}}
                        {{--<img src="assets/images/users/user2.jpg" class="pull-left" alt="John Doe"/>--}}
                        {{--<span class="contacts-title">John Doe</span>--}}
                        {{--<p>Praesent placerat tellus id augue condimentum</p>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                        {{--<div class="list-group-status status-away"></div>--}}
                        {{--<img src="assets/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk"/>--}}
                        {{--<span class="contacts-title">Dmitry Ivaniuk</span>--}}
                        {{--<p>Donec risus sapien, sagittis et magna quis</p>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="panel-footer text-center">--}}
                        {{--<a href="pages-messages.html">Show all messages</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</li>--}}
            <!-- END MESSAGES -->
            <!-- TASKS -->
            {{--<li class="xn-icon-button pull-right" id="notification_lists">--}}
            @if(isset($report_notifications))
                <li class="xn-icon-button pull-right">
                    <a href="#"><span class="fa fa-tasks"></span></a>
                    <div class="informer informer-warning" id="report_count">{{$report_notifications->count()}}</div>
                    <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="fa fa-tasks"></span> Notifications</h3>
                            <div class="pull-right"><span class="label label-warning"><span id="report_count_unread">{{$report_notifications->count()}}</span> Unread</span></div>
                        </div>
                        <div class="panel-body list-group list-group-contacts scroll" style="height: 350px;" id="each_notification">
                            @if($report_notifications->count() > 0)
                                @foreach($report_notifications->get() as $notification)
                                    <a class="list-group-item" href="/reports/show/{{$hashIds->encode($notification->report_id)}}">
                                        <img src="{!! ($notification->mobileUser()->first()->avatar) ? $notification->mobileUser()->first()->avatar
                                        : '/assets/images/users/no-image.jpg' !!}" style="width:30px;" class="pull-left img-circle">
                                        <p>{{$notification->title}}</p>
                                        <small class="text-muted">
                                            {{$notification->mobileUser()->first()->fullNames()}}
                                            {{($notification->created_at) ? $notification->created_at->format('D, jS, M Y') : ''}}
                                            ({{($notification->created_at) ? $notification->created_at->diffForHumans() : ''}})
                                        </small>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                        <div class="panel-footer text-center">
                            <a href="#">Show all tasks</a>
                        </div>
                    </div>
                </li>
            @endif
            <!-- END TASKS -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            @yield('breadcrumb')

            <li><i class="fa fa-calendar"></i> <?php echo '  ' . date("D, jS M, Y");  ?></li>
            <li><i class="glyphicon glyphicon-time"></i> <span id="timer"></span></li>
        </ul>
        <!-- END BREADCRUMB -->

        <!-- start: FLASH MESSAGE -->
        @if(Session::has('flash_message'))
            {!! Session::get('flash_message') !!}
        @endif

        <div class="alert hide" id="msg_box" role="alert">

        </div>
        <!-- end: FLASH MESSAGE -->

        @yield('content')

    </div>
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>
                <p>Press No if you want to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- START PRELOADS -->
{{--<audio id="audio-alert" src="{{ asset('/audio/alert.mp3') }}" preload="auto"></audio>--}}
{{--<audio id="audio-fail" src="{{ asset('/audio/fail.mp3') }}" preload="auto"></audio>--}}
<!-- END PRELOADS -->

<!-- START SCRIPTS -->
<!-- START PLUGINS -->
<script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>

<script type='text/javascript' src="{{ asset('/js/plugins/noty/jquery.noty.js') }}"></script>
<script type='text/javascript' src="{{ asset('/js/plugins/noty/layouts/topRight.js') }}"></script>
<script type='text/javascript' src="{{ asset('/js/plugins/noty/themes/default.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('/socket.io/socket.io.js') }}"></script>--}}

<!-- END PLUGINS -->

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="{{ asset('/js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/actions.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/custom/function.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/custom/reports/notification.js') }}"></script>
<!-- END PAGE PLUGINS -->

<!-- start: CUSTOM SCRIPT REQUIRED FOR THIS PAGE ONLY -->
@yield('custom_script')
        <!-- end: CUSTOM SCRIPT REQUIRED FOR THIS PAGE ONLY -->

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
//        $('#home_tab a:first').tab('show');
    });
</script>

<!-- END SCRIPTS -->

</body>
</html>