<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="/">{{env('APP_NAME')}}</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini p_image">
                @if(!Auth::user()->avatar)
                    <img src="{{ asset('/images/avatar.svg') }}" alt="{{Auth::user()->fullNames()}}"/>
                @else
                    <img src="{{ env('AMAZON_PATH') . Auth::user()->avatar }}" alt="{{Auth::user()->fullNames()}}"/>
                @endif
            </a>
            <div class="profile">
                <div class="profile-image p_image">
                    @if(!Auth::user()->avatar)
                        <img src="{{ asset('/images/avatar.svg') }}" alt="{{Auth::user()->fullNames()}}"/>
                    @else
                        <img src="{{ env('AMAZON_PATH') . Auth::user()->avatar }}" alt="{{Auth::user()->fullNames()}}"/>
                    @endif
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">{{Auth::user()->simpleNameNSalutation()}}</div>
                    <div class="profile-data-title">{{ Auth::user()->userType()->first()->user_type }}</div>
                </div>
                <div class="profile-controls">
                    <a href="/profiles" class="profile-control-left"><span class="fa fa-info"></span></a>
                </div>
            </div>
        </li>
        <li class="xn-title">Navigation</li>
        <li class="active">
            <a href="/"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
        </li>

        <?php $show_menu = []?>
        {{--Loop Through The Users Roles--}}
        @foreach(Auth::user()->roles()->get() as $role)
            {{--Loop Through The Menus --}}
            @foreach($active_menus as $menu)
                {{--  Check if the menu has been displayed--}}
                @if(!in_array($menu->menu_id, $show_menu))

                    {{--  Check if the logged in user have access to view the menu--}}
                    @if(in_array($menu->pledge_menu_id, $role->pledgeMenus()->get()->lists('pledge_menu_id')->toArray()))
                        <?php $show_menu[] = $menu->pledge_menu_id?>

                        {{--  Displays the menus--}}
                        @if($menu->pledgeMenuItems()->count() > 0)
                            <li class="xn-openable">
                                <a href="#"><span class="{{ $menu->pledge_icon }}"></span> <span class="xn-text">{{ $menu->pledge_menu }}</span></a>
                                <ul>

                                    <?php $show_menu_item = []?>
                                    {{--  Loop Through The Menu Items--}}
                                    @foreach($menu->pledgeMenuItems()->orderBy('sequence')->get() as $menu_item)
                                        {{--  Check if the menu Item has been displayed and its enabled--}}
                                        @if(!in_array($menu_item->pledge_menu_item_id, $show_menu_item) and $menu_item->active === 1)

                                            {{--Check if the logged in user have access to view the menu item --}}
                                            @if(in_array($menu_item->pledge_menu_item_id, $role->pledgeMenuItems()->get()->lists('pledge_menu_item_id')->toArray()))
                                                <?php $show_menu_item[] = $menu_item->pledge_menu_item_id?>

                                                {{--  Displays the menu items--}}
                                                @if($menu_item->pledgeSubMenuItems()->count() > 0)
                                                    <li class="xn-openable">
                                                        <a href="#"><span class="{{ $menu_item->pledge_menu_item_icon }}"></span> <span class="xn-text">{{ $menu_item->pledge_menu_item }}</span></a>
                                                        <ul>

                                                            <?php $show_sub_menu_item = []?>
                                                            {{--  Loop Through The Sub Menu Items--}}
                                                            @foreach($menu_item->pledgeSubMenuItems()->orderBy('sequence')->get() as $sub_menu_item)

                                                                {{--  Check if the Sub menu Item has been displayed and its enabled--}}
                                                                @if(!in_array($sub_menu_item->pledge_sub_menu_item_id, $show_sub_menu_item) and $sub_menu_item->active === 1)

                                                                    {{--Check if the logged in user have access to view the sub menu item --}}
                                                                    @if(in_array($sub_menu_item->pledge_sub_menu_item_id, $role->pledgeSubMenuItems()->get()->lists('pledge_sub_menu_item_id')->toArray()))
                                                                        <?php $show_sub_menu_item[] = $sub_menu_item->pledge_sub_menu_item_id?>

                                                                        {{--  Displays the sub menu items--}}
                                                                        <li>
                                                                            <a href="{{ $sub_menu_item->pledge_sub_menu_item_url }}">
                                                                                <span class="{{ $sub_menu_item->pledge_sub_menu_item_icon }}"></span> {{ $sub_menu_item->pledge_sub_menu_item }}
                                                                            </a>
                                                                        </li>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @else
                                                    <li><a href="{{ $menu_item->pledge_menu_item_url }}"><span class="{{ $menu_item->pledge_menu_item_icon }}"></span> {{ $menu_item->pledge_menu_item }}</a></li>
                                                @endif
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @else
                            <li>
                                <a href="{{ $menu->pledge_menu_url }}"><span class="{{ $menu->pledge_icon }}"></span> <span class="xn-text">{{ $menu->pledge_menu }}</span></a>
                            </li>
                        @endif
                    @endif
                @endif
            @endforeach
        @endforeach

        <li><a href="/users/change"><span class="fa fa-lock"></span> <span class="xn-text"> Change Password</span></a></li>
        <li><a href="/auth/logout"><span class="fa fa-power-off text-danger"></span> <span class="xn-text"> Logout</span></a></li>
    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->