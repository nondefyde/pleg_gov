@extends('layouts.default')

@section('title', 'Report Detail')

@section('breadcrumb')
    <li class="active"><a href="/reports">Reports</a></li>
@endsection

@section('content')
<!-- PAGE CONTENT WRAPPER -->
    <!-- END PAGE CONTENT WRAPPER -->

    <!-- START CONTENT FRAME -->
    <div class="content-frame">

        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">
            <div class="page-title">
                <h3>
                    <span class="fa fa-book"></span> {{$report->title}}
                    <label class="{{$report->governmentReport()->first()->getStatus()->first()->label_class}}">
                        {{$report->governmentReport()->first()->getStatus()->first()->status}}
                    </label>
                    ({{ $report->state()->first()->state }} State)
                </h3>
            </div>
        </div>

        <!-- START CONTENT FRAME RIGHT -->
        <div class="content-frame-right">
            <h4>Reports:</h4>
            <div class="list-group border-bottom push-down-20">
                @if($user->reportSectors()->count() > 0)
                    <a href="#" class="list-group-item active">Sectors</a>
                    @foreach($user->reportSectors()->get() as $sector)
                        <a href="{{ url('/reports/sectors/'.$hashIds->encode($sector->sector_id)) }}"  class="list-group-item ">
                            {{$sector->sector}} Sector<span class="badge badge-info">{{$user->getReportSectors($sector->sector_id)->count()}}</span>
                        </a>
                    @endforeach
                @endif

                {{--@if($user->reportStates()->count() > 0)--}}
                    {{--<a href="#" class="list-group-item active">States</a>--}}
                    {{--@foreach($user->reportStates()->get() as $state)--}}
                        {{--<a href="{{ url('/reports/states/'.$hashIds->encode($state->state_id)) }}"  class="list-group-item">--}}
                            {{--{{$state->state}}<span class="badge badge-success">{{$user->getReportStates($state->state_id)->count()}}</span>--}}
                        {{--</a>--}}
                    {{--@endforeach--}}
                {{--@endif--}}

            </div>
            {{--<h4>Tags:</h4>--}}
            {{--<ul class="list-tags">--}}
                {{--<li><a href="#"><span class="fa fa-tag"></span> Lagos State</a></li>--}}
                {{--<li><a href="#"><span class="fa fa-tag"></span> Alimosho Local Government</a></li>--}}
                {{--<li><a href="#"><span class="fa fa-tag"></span> Power Sector</a></li>--}}
            {{--</ul>--}}
        </div>

        <!-- END CONTENT FRAME RIGHT -->

        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-body content-frame-body-left">
            @if($report->images())
                <div class="gallery" id="links">
                    @for($i = 0; $i < count($report->images()); $i++)
                        <a class="gallery-item" href="{{asset($report->file_path.$report->images()[$i])}}" title="{{$report->title}}" data-gallery>
                            <div class="image">
                                <img src="{{asset($report->file_path.$report->images()[$i])}}" alt="..."/>
                            </div>
                        </a>
                    @endfor
                </div>
            @endif
            {{--@if($report->images()->count() > 0)--}}
                {{--<div class="gallery" id="links">--}}
                    {{--@foreach($report->images()->get() as $image)--}}
                        {{--<a class="gallery-item" href="{{asset($image->file_path.$image->image_url)}}" title="{{$report->title}}" data-gallery>--}}
                            {{--<div class="image">--}}
                                {{--<img src="{{asset($image->file_path.$image->image_url)}}" alt="..."/>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--@endif--}}
            <div class="panel">
                @if($report->description)
                    <div class="panel-heading">
                        <h4>Description</h4>
                    </div>
                    <div class="panel-body">
                        <p>{{$report->description}}</p>
                    </div>
                @endif
                <div class="panel-footer">
                    <div class="col-xs-12 col-md-12">
                        <div class="col-xs-6 col-md-6">
                            <strong>55 people Backed this report</strong>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            Reported By <img src="{{($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg'}}"
                                 style="width:30px;" alt="{{$report->mobileUser()->first()->first_name}}" class="img-circle">
                            <button value="{{$report->mobileUser()->first()->mobile_user_id}}" class="btn btn-link reported_by">
                                {{$report->mobileUser()->first()->fullNames()}}
                            </button>
                            <div id="loader" class="hide pull-right">
                                <img class="img-circle" style="width:20px;" src="/images/loading.gif" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CONTACTS -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Backers <span class="badge"> 55</span></h3>
                </div>
                <div class="panel-body list-group list-group-contacts">
                    <a href="#" class="list-group-item">
                        <img src="/assets/images/users/user3.jpg" class="pull-left" alt="Nadia Ali"/>
                        <span class="contacts-title">Ben Ikeji</span>
                        <p>beno@gmail.com</p>
                        <div class="list-group-controls">
                            <button class="btn btn-primary btn-rounded btn-condensed btn-sm"
                                    data-toggle="modal" data-target="#modal_basic"><span class="fa fa-info"></span> Details
                            </button>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- END CONTENT FRAME BODY -->
    </div>
    <!-- END CONTENT FRAME -->
    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">&lsaquo;</a>
        <a class="next">&rsaquo;</a>
        <a class="close">&times;</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- END BLUEIMP GALLERY -->

    <!-- MODALS -->
    <div class="modal" id="reported_by_modal" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="reported_by_modal_content">

            </div>
        </div>
    </div>


    <div class="modal" id="modal_backer" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="defModalHead">
                        <img src="assets/images/users/user2.jpg" style="width:30px" class="img-circle"/> Okafor Emmanuel</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6 col-md-4">
                            <div class="text-center" id="user_image">
                                <img src="assets/images/users/user2.jpg" class="img-thumbnail"/>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-8">
                            <table class="table table-bordered">
                                <tr>
                                    <td>First Name</td>
                                    <td>Emmanuel</td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td>Okafor</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>Male</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>nondefyde@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>Mobile</td>
                                    <td>08061539278</td>
                                </tr>
                                <tr>
                                    <td>Sate</td>
                                    <td>Lagos</td>
                                </tr>
                                <tr>
                                    <td>L.G.A</td>
                                    <td>Alimosho Local Government</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ url('/activity') }}" class="btn btn-default pull-left"> <span class="fa fa-clock-o"></span> Activity</a>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODALS -->



@endsection

@section('custom_script')
    <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom/reports/report.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/reports"]');

            document.getElementById('links').onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement;
                var link = target.src ? target.parentNode : target;
                var options = {
                    index: link, event: event, onclosed: function () {
                        setTimeout(function () {
                            $("body").css("overflow", "");
                        }, 200);
                    }
                };
                var links = this.getElementsByTagName('a');
                blueimp.Gallery(links, options);
            };
        });

    </script>
@endsection