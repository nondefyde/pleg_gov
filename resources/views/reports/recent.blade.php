@extends('layouts.default')

@section('title', 'Recent Reports')

@section('breadcrumb')
    <li class="active"><a href="/reports">Reports</a></li>
    <li class="active">Recent</li>
@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->

    <div class="page-title">
        <h2><span class="fa fa-clock-o"></span> Recent Reports</h2>
    </div>

    <div class="page-content-wrap">

        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-12">
                <!-- START PANEL WITH REMOVE CALLBACKS -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="list-tags">
                                    @if($user->reportSectors()->count() > 0)
                                        @foreach($user->reportSectors()->get() as $sector)
                                            <li>
                                                <a href="{{ url('/reports/sectors/'.$hashIds->encode($sector->sector_id)) }}">
                                                    <span class="fa fa-tag"> {{ $sector->sector }}</span>
                                                    ({{$user->getReportSectors($sector->sector_id)->count()}})
                                                </a>
                                            </li>
                                        @endforeach
                                    @else
                                        @foreach(Sector::orderBy('sector')->get() as $sector)
                                            <li>
                                                <a href="{{ url('/reports/sectors/'.$hashIds->encode($sector->sector_id)) }}">
                                                    <span class="fa fa-tag"> {{ $sector->sector }}</span>
                                                    ({{$user->getReportSectors($sector->sector_id)->count()}})
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PANEL WITH REMOVE CALLBACKS -->
            </div>
        </div>
        <!-- END WIDGETS -->

        <!-- START RESPONSIVE TABLES -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Reports</h3>
                        <div id="loader" class="hide text-center">
                            <img class="img-circle" style="width:30px;" src="/images/loading.gif" alt="...">
                        </div>
                    </div>
                    <div class="panel-body panel-body-table">
                        <div class="table-responsive">
                            <table class="table table-bordered table-actions datatable">
                                <thead>
                                    <tr>
                                        <th style="width:1%;">#</th>
                                        <th style="width:5%;">Photo</th>
                                        <th style="width:16%;">Name</th>
                                        <th style="width:40%;">Report Title</th>
                                        <th style="width:6%;">State</th>
                                        <th style="width:8%;">Sector</th>
                                        <th style="width:4%;">Backers</th>
                                        <th style="width:4%;">Summary</th>
                                        <th style="width:4%;">Detail</th>
                                        @if(UserType::EXECUTIVES === $user->user_type_id)
                                            <th style="width:4%;">Status</th>
                                        @else
                                            <th style="width:4%;">Forward</th>
                                            <th style="width:4%;">Archive</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody id="report_tbody">
                                    @if(count($reports) > 0)
                                        <?php $i = 1; ?>
                                        @foreach($reports as $report)
                                            <tr>
                                                <td class="text-center">{{ $i++ }}</td>
                                                <td>
                                                    <img src="{{($report->mobileUser()->first()->avatar) ? $report->mobileUser()->first()->avatar : '/assets/images/users/no-image.jpg'}}" style="width:30px;" class="img-circle">
                                                </td>
                                                <td>
                                                    <strong>
                                                        <button value="{{$report->mobileUser()->first()->mobile_user_id}}" class="btn btn-link reported_by">
                                                            {{$report->mobileUser()->first()->fullNames()}}
                                                        </button>
                                                    </strong>
                                                </td>
                                                <td>{{ $report->title }}</td>
                                                <td>{{ $report->state()->first()->state }}</td>
                                                <td>{{ $report->sector()->first()->sector }}</td>
                                                <td><span class="badge badge-primary">55</span></td>
                                                <td>
                                                    <button value="{{$report->report_id}}" class="btn btn-info btn-rounded btn-condensed btn-sm report_detail">
                                                        <span class="fa fa-info-circle"></span>
                                                    </button>
                                                </td>
                                                <td>
                                                    <a href="{{ url('/reports/show/'.$hashIds->encode($report->report_id)) }}" class="btn btn-default btn-rounded btn-condensed btn-xs">
                                                        <span class="fa fa-eye-slash"></span>
                                                    </a>
                                                </td>
                                                @if(UserType::EXECUTIVES === $user->user_type_id)
                                                    <td>
                                                        <label class="{{$report->governmentReport()->first()->getStatus()->first()->label_class}}">
                                                            {{$report->governmentReport()->first()->getStatus()->first()->status}}
                                                        </label>
                                                    </td>
                                                @else
                                                    <td>
                                                        <button  value="{{$report->report_id}}"class="btn btn-primary btn-rounded btn-condensed btn-sm forward_report">
                                                            <span class="fa fa-send"></span>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button value="{{$report->report_id}}" class="btn btn-danger btn-rounded btn-condensed btn-sm archive_report">
                                                            <span class="fa fa-file-archive-o"></span>
                                                        </button>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- END RESPONSIVE TABLES -->

    </div>

    <!-- MODALS -->
    <div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal_content">

            </div>
        </div>
    </div>

    <div class="modal" id="reported_by_modal" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="reported_by_modal_content">

            </div>
        </div>
    </div>

    <div class="modal" id="forward_report_modal" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <form method="post" action="{{ url('/reports/forward-report') }}" class="form-horizontal" role="form">
                {!! csrf_field() !!}
                <div class="modal-content" id="forward_report_modal_content">

                </div>
            </form>
        </div>
    </div>
    <!-- END MODALS -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="confirm-forward-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span>You are about to <strong><span id="forward_name"></span></strong> Report?
                </div>
                <div class="mb-content">
                    <p>Press Yes to Continue.</p>
                    <p>Press No to cancel.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes" id="forward_status">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="message-box animated fadeIn" data-sound="alert" id="confirm-archive-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span>You are about to <strong><span id="archive_name"></span></strong> Report?
                </div>
                <div class="mb-content">
                    <p>Press Yes to Continue.</p>
                    <p>Press No to cancel.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes" id="archive_status">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

@endsection

@section('custom_script')
    <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom/reports/report.js') }}"></script>

    <!-- END THIS PAGE PLUGINS-->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/reports/recent"]');
        });
    </script>
@endsection