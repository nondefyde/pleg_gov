@extends('layouts.default')

@section('title', '404 Page Not Found')

@section('breadcrumb')
    <li><a href="#">Error</a></li>
    <li class="active">Error: 404</li>
@endsection

@section('content')
    <div class="page-title">
        <h2><span class="fa fa-unlink"></span> 404 Page Not Found</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">

                <div class="error-container">
                    <div class="error-code"><span class="fa fa-unlink"></span> 404</div>
                    <div class="error-text">Sorry, this page isn't available.</div>
                    <div class="error-subtext">
                        Unfortunately we're having trouble loading the page you are looking for.
                        The link you followed may be broken, or the page may have been removed.
                        Please wait a moment and try again or use navigation side bar.
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->

@endsection

@section('custom_script')
    <script>
        jQuery(document).ready(function() {
            setTabActive('[href="/dashboard"]');
        });
    </script>
@endsection