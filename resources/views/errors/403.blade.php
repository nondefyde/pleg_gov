@extends('layouts.default')

@section('title', 'Error 403')

@section('breadcrumb')
    <li><a href="#">Error</a></li>
    <li class="active">Error: 403</li>
@endsection

@section('content')
    <div class="page-title">
        <h2><span class="fa fa-lock"></span> Error 403: Forbidden, Access Denied</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">

                <div class="error-container">
                    <div class="error-code">403</div>
                    <div class="error-text">Forbidden, Access Denied Error</div>
                    <div class="error-subtext"><strong>Whoops!!! You have no access / privilege to perform such action / operation.</strong>
                    <br>The request was a valid request, but the server is refusing to respond to it</div>

                </div>

            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->

@endsection

@section('custom_script')
    <script>
        jQuery(document).ready(function() {
            setTabActive('[href="/dashboard"]');
        });
    </script>
@endsection