@extends('layouts.default')

@section('title', 'User')

@section('breadcrumb')
    <li class="active"><a href="/reports">User</a></li>
    @endsection

    @section('content')
            <!-- PAGE CONTENT WRAPPER -->
    <div class="page-title">
        <h2><span class="fa fa-users"></span> Manage Users</h2>
    </div>

    <div class="page-content-wrap">
        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-12">
                <!-- START PANEL WITH REMOVE CALLBACKS -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-7">
                                <form action="#" class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input id="quick-search" class="form-control" placeholder="Search Keyword"/>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary ">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PANEL WITH REMOVE CALLBACKS -->
            </div>
        </div>
        <!-- END WIDGETS -->


        <!-- START RESPONSIVE TABLES -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registered Users</h3>
                    </div>
                    <div class="panel-body panel-body-table">
                        <div class="table-responsive">
                            <table class="table table-bordered table-actions dataTable">
                                <thead>
                                <tr>
                                    <th style="width:1%;">s/no</th>
                                    <th style="width:1%;">Avatar</th>
                                    <th style="width:15%;">First name</th>
                                    <th style="width:15%;">Last Name</th>
                                    <th style="width:15%;">E:mail</th>
                                    <th style="width:10%;">Profile</th>
                                    <th style="width:5%;">Activity</th>
                                    <th style="width:5%;">Deactivate</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="trow_1">
                                    <td class="text-center">1</td>
                                    <td>
                                        <img src="assets/images/users/user2.jpg" style="width:30px;" alt="John Doe"
                                             class="img-circle">
                                    </td>
                                    <td><strong>Emmanuel</strong></td>
                                    <td><strong>Okafor</strong></td>
                                    <td>nondefyde@gmail.com</td>
                                    <td>
                                        <button class="btn btn-default btn-rounded btn-condensed btn-sm"
                                                data-toggle="modal" data-target="#modal_basic"><span
                                                    class="fa fa-info"></span> Detail
                                        </button>
                                    </td>
                                    <td>
                                        <a href="{{ url('/activity') }}" class="btn btn-default btn-rounded btn-condensed btn-sm"><span
                                                    class="fa fa-clock-o"></span> Activity
                                        </a>
                                    </td>
                                    <td>
                                        <button class="btn btn-default btn-rounded btn-condensed btn-sm"><span
                                                    class="fa fa-lock"></span> Deactivate
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <ul class="pagination pagination-sm pull-right">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li class="active"><a href="#">3</a></li>
                            <li class="disabled"><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <!-- END RESPONSIVE TABLES -->

    </div>

    <!-- MODALS -->
    <div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="defModalHead">
                        <img src="assets/images/users/user2.jpg" style="width:30px" class="img-circle"/> Okafor Emmanuel</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6 col-md-4">
                            <div class="text-center" id="user_image">
                                <img src="assets/images/users/user2.jpg" class="img-thumbnail"/>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-8">
                            <table class="table table-bordered">
                                <tr>
                                    <td>First Name</td>
                                    <td>Emmanuel</td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td>Okafor</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>Male</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>nondefyde@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>Mobile</td>
                                    <td>08061539278</td>
                                </tr>
                                <tr>
                                    <td>Sate</td>
                                    <td>Lagos</td>
                                </tr>
                                <tr>
                                    <td>L.G.A</td>
                                    <td>Alimosho Local Government</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('custom_script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/reports"]');
        });
    </script>
@endsection