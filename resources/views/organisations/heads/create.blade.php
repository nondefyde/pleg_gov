@extends('layouts.default')

@section('title', 'Add Organisation Head')

@section('breadcrumb')
    <li><a href="/organisation-heads">Organisation Heads</a></li>
    <li class="active">Create</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-plus-square"></span> Add Organisation Head</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                @include('errors.errors')
                <form method="post" action="{{ url('/organisation-heads/create') }}" class="form-horizontal" role="form">
                    {!! csrf_field() !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>New Head of Organisation</strong> Form</h3>
                            <ul class="panel-controls">
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <p></p>
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">First Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" value="{{ Input::old('first_name') }}" required name="first_name" placeholder="Organisation's Head First Name" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Last Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" value="{{ Input::old('last_name') }}" required name="last_name" placeholder="Organisation's Head Last Name" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Gender</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <div class="col-md-4">
                                            <label class="check"><input type="radio" value="Female" class="icheckbox" name="gender"/> Female</label>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="check"><input type="radio" value="Male" class="iradio" name="gender"/> Male</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">E-mail</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="text" value="{{ Input::old('email') }}" required name="email" placeholder="Organisation's Head E-mail" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Mobile Number</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="text" value="{{ Input::old('mobile') }}" name="mobile" placeholder="Organisation's Head Phone Number" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default" type="reset">Clear Form</button>
                            <button class="btn btn-primary pull-right" type="submit">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    @endsection

    @section('custom_script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/organisation-heads/create"]');
        });
    </script>
@endsection