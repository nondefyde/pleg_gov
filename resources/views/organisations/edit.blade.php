@extends('layouts.default')

@section('title', 'Edit Organisation')

@section('breadcrumb')
    <li><a href="/organisations">Organisation</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-plus-square"></span> Edit Organisation {{ $organisation->name }}</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <div>@include('errors.errors')</div>
                <form role="form" action="{{ url('/organisations/edit/' . $hashIds->encode($organisation->organisation_id)) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PATCH">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Edit Organisation</strong> Form</h3>
                        </div>
                        <div class="panel-body">
                            <p></p>
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisation Sector <small class="text-danger">*</small></label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="sector_id" class="form-control select" required>
                                        <option value="">Nothing Selected</option>
                                        @foreach($sectors as $sector)
                                            @if($sector->sector_id === $organisation->sector_id)
                                                <option selected value="{{$sector->sector_id}}">{{$sector->sector}}</option>
                                            @else
                                                <option value="{{$sector->sector_id}}">{{$sector->sector}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">State </label>

                                <div class="col-md-6 col-xs-12">
                                    @if($lga === null)
                                        {!! Form::select('state_id', $states, Input::old('state_id'), ['class'=>'form-control', 'id'=>'state_id']) !!}
                                    @else
                                        {!! Form::select('state_id', $states, $lga->state_id, ['class'=>'form-control', 'id'=>'state_id']) !!}
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">L.G.A </label>

                                <div class="col-md-6 col-xs-12">
                                    @if($lga == null)
                                        {!! Form::select('lga_id', [''=>'Nothing Selected'], '', ['class'=>'form-control', 'id'=>'lga_id']) !!}
                                    @else
                                        {!! Form::select('lga_id', $lgas, $lga->lga_id, ['class'=>'form-control', 'id'=>'lga_id']) !!}
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisational Head <small class="text-danger">*</small></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <select class="form-control" name="organisation_head_id">
                                            <option value="">Nothing Selected</option>
                                            @foreach($organisation_heads as $user)
                                                @if($user->child_user_id === $organisation->organisation_head_id)
                                                    <option selected value="{{$user->child_user_id}}">{{$user->organisationHead()->first()->simpleName()}}</option>
                                                @else
                                                    <option value="{{$user->child_user_id}}">{{$user->organisationHead()->first()->simpleName()}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <a class="btn btn-default" href="/organisation-heads/create">Add New</a>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Responsibility </label>

                                <div class="col-md-6 col-xs-12">
                                    {!! Form::textarea('responsibility', $organisation->responsibility, ['class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Organisation head\'s Responsibility']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisation Name <small class="text-danger">*</small></label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('name', $organisation->name, ['id'=>'name', 'placeholder'=>'Description', 'class'=>'form-control', 'required'=>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description <small class="text-danger">*</small></label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('description', $organisation->description, ['id'=>'description', 'placeholder'=>'Description', 'class'=>'form-control', 'required'=>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">E-mail <small class="text-danger">*</small></label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        {!! Form::text('email', $organisation->email, ['id'=>'email', 'placeholder'=>'E:mail', 'class'=>'form-control', 'required'=>'required']) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Phone Number</label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        {!! Form::text('phone_no', $organisation->mobile, ['id'=>'mobile', 'placeholder'=>'Phone No', 'class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">C.A.C Registration No</label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('cac_registration_no', $organisation->cac_registration_no, ['id'=>'cac_registration_no', 'placeholder'=>'CAC Registration No', 'class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address <small class="text-danger">*</small></label>

                                <div class="col-md-6 col-xs-12">
                                    {!! Form::textarea('address', $organisation->address, ['class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Office Address']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisation's Logo </label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="file" class="fileinput btn-primary" placeholder="Organisation's Logo (jpeg,gif,png)" name="logo_url" id="logo_url" title="Browse Logo (jpeg,gif,png)"/>
                                    <img class="pull-right" id="logo_preview" src="{{ ($organisation->logo_url) ? $organisation->fullUploadPath() : asset('/assets/images/users/no-image.jpg') }}"
                                         style="width: 150px; height: 100px;" alt="Organisation's Logo"/>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default" type="reset">Clear Form</button>
                            <button class="btn btn-primary pull-right" type="submit">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('custom_script')

    <script type="text/javascript" src="{{ asset('/js/plugins/jquery/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/form/jquery.form.js') }}"></script>

    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/custom/organisation.js') }}"></script>
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/organisations/create"]');
        });
    </script>
@endsection