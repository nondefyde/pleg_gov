@extends('layouts.default')

@section('title', 'Approved Reports')

@section('breadcrumb')
    <li class="active"><a href="/reports">Reports</a></li>
    <li class="active">Manage</li>
    @endsection

    @section('content')
            <!-- PAGE CONTENT WRAPPER -->
    <div class="page-title">
        <h2><span class="fa fa-send-o"></span> Approved Reports</h2>
    </div>

    <div class="page-content-wrap">

        <!-- START WIDGETS -->
        <!-- END WIDGETS -->

        <!-- START RESPONSIVE TABLES -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Reports</h3>
                        <div id="loader" class="hide text-center">
                            <img class="img-circle" style="width:30px;" src="/images/loading.gif" alt="...">
                        </div>
                    </div>
                    <div class="panel-body panel-body-table">
                        <div class="table-responsive">
                            <table class="table table-bordered table-actions datatable">
                                <thead>
                                <tr>
                                    <th style="width:1%;">#</th>
                                    <th style="width:5%;">Photo</th>
                                    <th style="width:22%;">Name</th>
                                    <th style="width:42%;">Report Title</th>
                                    <th style="width:10%;">Sector</th>
                                    <th style="width:5%;">Backers</th>
                                    <th style="width:5%;">Summary</th>
                                    <th style="width:5%;">Detail</th>
                                    <th style="width:5%;">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($reports) > 0)
                                    <?php $i = 1; ?>
                                    @foreach($reports as $report)
                                        <tr>
                                            <td class="text-center">{{ $i++ }}</td>
                                            <td>
                                                <img src="/assets/images/users/user2.jpg" style="width:30px;" alt="John" class="img-circle">
                                            </td>
                                            <td><strong><a href="#">{{ $report->mobileUser()->first()->fullNames() }}</a></strong></td>
                                            <td>{{ $report->title }}</td>
                                            <td>{{ $report->sector()->first()->sector }}</td>
                                            <td><span class="badge badge-primary">55</span></td>
                                            <td>
                                                <button value="{{$report->report_id}}" class="btn btn-info btn-rounded btn-condensed btn-sm report_detail">
                                                    <span class="fa fa-info-circle"></span> View
                                                </button>
                                            </td>
                                            <td>
                                                <a href="{{ url('/organisation-reports/show/'.$hashIds->encode($report->report_id)) }}" class="btn btn-default btn-rounded btn-condensed btn-xs">
                                                    <span class="fa fa-eye-slash"></span>
                                                </a>
                                            </td>
                                            <td>
                                                <label class="label label-success">Approved</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- END RESPONSIVE TABLES -->
    </div>

    <!-- MODALS -->
    <div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal_content">

            </div>
        </div>
    </div>
    <!-- END MODALS -->

    @endsection

    @section('custom_script')
            <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom/reports/report.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/organisation-reports/approved"]');
        });
    </script>
@endsection