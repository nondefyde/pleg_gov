@extends('layouts.default')

@section('title', 'Add Organisation')

@section('breadcrumb')
    <li><a href="/organisations">Organisation</a></li>
    <li class="active">Create</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-plus-square"></span> Add Organisation</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                @include('errors.errors')
                <form method="post" action="{{ url('/organisations/create') }}" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input name="user_id" type="hidden" value="{{Auth::user()->user_id}}">
                    {!! csrf_field() !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>New Organisation</strong> Form</h3>
                        </div>
                        <div class="panel-body">
                            <p></p>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisation Sector <small class="text-danger">*</small></label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="sector_id" class="form-control select" required>
                                        <option value="">Nothing Selected</option>
                                        @foreach($sectors as $sector)
                                            <option value="{{$sector->sector_id}}">{{$sector->sector}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">State</label>
                                <div class="col-md-6 col-xs-12">
                                    {!! Form::select('state_id', $states, "", ['class'=>'form-control select', 'id'=>'state_id']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">L.G.A</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control" name="lga_id" id="lga_id">
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisation Name <small class="text-danger">*</small></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" value="{{ Input::old('name') }}" name="name" required
                                               placeholder="Organisation" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisational Head <small class="text-danger">*</small></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <select class="form-control" name="head_user_id">
                                            <option value="">Nothing Selected</option>
                                            @foreach($organisation_heads as $user)
                                                <option value="{{$user->child_user_id}}">{{$user->childUser()->first()->simpleName()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <a class="btn btn-default" href="/organisation-heads/create">Add New</a>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Responsibility </label>

                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control" rows="3" name="responsibility"
                                              placeholder="Organisation head's Responsibility">{{ Input::old('responsibility') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description <small class="text-danger">*</small></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" value="{{ Input::old('description') }}" name="description" required
                                               placeholder="few word Description" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">E-mail <small class="text-danger">*</small></label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="text" value="{{ Input::old('email') }}" name="email" required
                                               placeholder="Organisation E-mail" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Phone Number</label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="text" value="{{ Input::old('phone_no') }}" name="phone_no"
                                               placeholder="Organisation Phone Number" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">C.A.C Registration No</label>

                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" value="{{ Input::old('cac_registration_no') }}"
                                               name="cac_registration_no" placeholder="CAC Registration No"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address <small class="text-danger">*</small></label>

                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control" rows="3" name="address" required
                                              placeholder="Head Office Address">{{ Input::old('address') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Organisation's Logo </label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="file" class="fileinput btn-primary" placeholder="Organisation's Logo (jpeg,gif,png)" name="logo_url" id="logo_url" title="Browse Logo (jpeg,gif,png)"/>
                                    <img class="pull-right" id="logo_preview" src="{{ asset('/assets/images/users/no-image.jpg') }}" style="width: 150px; height: 100px;" alt="Organisation's Logo"/>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default" type="reset">Clear Form</button>
                            <button class="btn btn-primary pull-right" type="submit">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('custom_script')

    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>

    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/custom/organisation.js') }}"></script>
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/organisations/create"]');
        });
    </script>
@endsection