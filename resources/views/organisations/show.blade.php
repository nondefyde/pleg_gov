@extends('layouts.default')

@section('title', 'Organisation Details')

@section('breadcrumb')
    <li><a href="/organisations">Organisations</a></li>
    <li class="active">Detail</li>
@endsection

@section('content')

    <div class="page-title">
        <h3><span class="fa fa-home"></span> Organisation {{ $organisation->name }}</h3>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        <h3>
                            <span class="fa fa-info-circle"></span> Organisation: {{ $organisation->name }}
                        </h3>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Organisation Sector : </label>

                                <div class="col-md-8 col-xs-7 line-height-30">{{ $organisation->sector()->first()->sector }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Organisation Head : </label>

                                <div class="col-md-8 col-xs-7 line-height-30">{{ $organisation->organisationHead()->first()->simpleName() }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Description : </label>

                                <div class="col-md-8 col-xs-7 line-height-30">{{$organisation->description }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">E-mail : </label>

                                <div class="col-md-8 col-xs-7 line-height-30">
                                    {!! ($organisation->email) ? $organisation->email : '<span class="label label-danger">nil</span>' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Phone Number : </label>

                                <div class="col-md-8 col-xs-7 line-height-30">
                                    {!! ($organisation->phone_no) ? $organisation->phone_no : '<span class="label label-danger">nil</span>' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">C.A.C Registration No: </label>

                                <div class="col-md-8 col-xs-7 line-height-30">
                                    {!! ($organisation->cac_registration_no) ? $organisation->cac_registration_no : '<span class="label label-danger">nil</span>' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Address : </label>

                                <div class="col-md-8 col-xs-7 line-height-30">
                                    {!! $organisation->address !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Organisation's Logo : </label>

                                <div class="col-md-8 col-xs-7 line-height-30">
                                    <img style="min-width:80px; height: 80px" src="{{ ($organisation->logo_url) ? $organisation->fullUploadPath() : asset('/images/avatar.svg') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('custom_script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/organisations"]');
        });
    </script>
@endsection