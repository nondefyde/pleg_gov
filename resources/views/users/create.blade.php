@extends('layouts.default')

@section('title', 'Register New User')

@section('breadcrumb')
    <li><a href="{{ url('/users/create') }}">User</a></li>
    <li class="active">Register</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-plus"></span> Register User</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                @include('errors.errors')
                <form method="POST" action="/users/create" accept-charset="UTF-8" class="form-horizontal" role="form">
                    {!! csrf_field() !!}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><span class="fa fa-pencil"></span> New User</h3>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">First Name</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="text" name="first_name" value="{{ old('first_name') }}"
                                           class="form-control" placeholder="First Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">Last Name</label>

                                <div class="col-md-9 col-xs-7">
                                    <input type="text" name="last_name" value="{{ old('last_name') }}"
                                           class="form-control" placeholder="Last Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">E-mail</label>

                                <div class="col-md-9 col-xs-7">
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control"
                                           placeholder="E-mail"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">Gender</label>
                                <div class="col-md-9 col-xs-7">
                                    <select name="gender" class="form-control select">
                                        <option value="">Noting Selected</option>
                                        <option value="Female">Female</option>
                                        <option value="Male">Male</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">User Type</label>
                                <div class="col-md-9 col-xs-7">
                                    <select name="user_type_id" class="form-control select">
                                        <option value="">Noting Selected</option>
                                        @foreach($user_types as $user_type)
                                            <option value="{{$user_type->user_type_id}}">{{$user_type->user_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-xs-5">
                                    <input class="btn btn-primary btn-rounded pull-right" type="submit" value="Sign Up User">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    @endsection


    @section('custom_script')
    <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->
    <script>
        jQuery(document).ready(function() {
            setTabActive('[href="/users/create"]');
        });
    </script>
@endsection