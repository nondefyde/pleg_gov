@extends('layouts.default')

@section('title', 'Manage Users')

@section('breadcrumb')
    <li><a href="/users">Users</a></li>
    <li class="active">Manage</li>
@endsection

@section('content')

    <div class="page-title">
        <h2><span class="fa fa-user"></span> Manage Users</h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">

        </div>
        <div class="col-md-12">
            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">List of Users</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-actions datatable">
                            <thead>
                                <tr>
                                    <th style="width: 1%;">#</th>
                                    <th style="width: 19%;">First Name</th>
                                    <th style="width: 19%;">Last Name</th>
                                    <th style="width: 20%;">Email</th>
                                    <th style="width: 5%;">Gender</th>
                                    <th style="width: 17%;">User Type</th>
                                    <th style="width: 9%;">Status</th>
                                    <th style="width: 5%;">View</th>
                                    <th style="width: 5%;">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($users) > 0)
                                <?php $i = 1; ?>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">{{$i++}} </td>
                                        <td>{{ $user->first_name }}</td>
                                        <td>{{ $user->last_name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{!! ($user->gender) ? $user->gender : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{{ $user->userType()->first()->user_type }}</td>
                                        <td>
                                            @if($user->status === 1)
                                                <button value="{{ $user->user_id }}" rel="0" class="btn btn-success btn-rounded btn-condensed btn-xs user_status">
                                                     Deactivate
                                                </button>
                                            @else
                                                <button value="{{ $user->user_id }}" rel="1" class="btn btn-danger btn-rounded btn-condensed btn-xs user_status">
                                                    Activate
                                                </button>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('/users/show/'.$hashIds->encode($user->user_id)) }}" class="btn btn-info btn-rounded btn-condensed btn-xs">
                                                <span class="fa fa-eye-slash"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ url('/users/edit/'.$hashIds->encode($user->user_id)) }}" class="btn btn-warning btn-rounded btn-condensed btn-xs">
                                                <span class="fa fa-edit"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="width: 1%;">#</th>
                                <th style="width: 19%;">First Name</th>
                                <th style="width: 19%;">Last Name</th>
                                <th style="width: 20%;">Email</th>
                                <th style="width: 5%;">Gender</th>
                                <th style="width: 17%;">User Type</th>
                                <th style="width: 9%;">Status</th>
                                <th style="width: 5%;">View</th>
                                <th style="width: 5%;">Edit</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="confirm-status-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span>You are about to <strong><span id="status_name"></span></strong> ?
                </div>
                <div class="mb-content">
                    <p>Press Yes to Continue.</p>
                    <p>Press No to cancel.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes" id="confirm_status">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

@endsection

@section('custom_script')
    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom/records/users.js') }}"></script>
    <!-- END TEMPLATE -->
    <script>
        jQuery(document).ready(function() {
            setTabActive('[href="/users"]');
        });
    </script>
@endsection