/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {

    $(document.body).on('click', '.delete_organisation',function(e){
        //e.preventDefault();
        var box = $("#confirm-remove-row");
        var parent = $(this).parent().parent();
        var organisation_id = $(this).val();
        var organisation_name = parent.children(':nth-child(2)').text();

        $("#organisation_name").text(organisation_name);
        $("#confirm_organisation_delete").val(organisation_id);
        box.addClass("open");
    });

    $(document.body).on('click', '#confirm_organisation_delete',function(e){
        //e.preventDefault();
        var box = $("#confirm-remove-row");
        var organisation_id = $(this).val();
        $.ajax({
            type: 'GET',
            async: true,
            url: '/organisations/delete/' + organisation_id,
            success: function(data,textStatus){
                box.removeClass("open");
                window.location.replace('/organisations');
            },
            error: function(xhr,textStatus,error){
                alert(textStatus + ' ' + xhr);
            }
        });
        return false;
    });

    //Preview Organisation Logo on Change of each input file
    $(document.body).on('change', '#logo_url',function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e)
            {
                $('#logo_preview').attr('src', e.target.result).width(150).height(100);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    //Preview Party Logo on Change of each input file
    $(document.body).on('change', '#avatar',function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e)
            {
                $('#avatar_preview').attr('src', e.target.result).width(130).height(120);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    // Ajax Get Local Governments Based on the state
    getDependentListBox($('#state_id'), $('#lga_id'), '/list-box/lga/');

});
