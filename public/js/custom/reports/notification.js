/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {

    ///////////////////////////////////////// Reports Notifications //////////////////////////////////////////////////////////////////
    //Get all the unread reports
    //$.ajax({
    //    type: "get",
    //    async: true,
    //    url: '/reports/unread',
    //    dataType: "html",
    //    success: function (data, textStatus) {
    //        var reports = JSON.parse(data);
    //        var output = '<a href="#"><span class="fa fa-tasks"></span></a>';
    //        output +=       '<div class="informer informer-warning" id="report_count">'+reports.length+'</div>';
    //        output +=           '<div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">';
    //        output +=               '<div class="panel-heading">';
    //        output +=                   '<h3 class="panel-title"><span class="fa fa-tasks"></span> Notifications</h3>';
    //        output +=                   '<div class="pull-right"><span class="label label-warning"><span id="report_count_unread">'+reports.length+'</span> Unread</span></div>';
    //        output +=               '</div>';
    //        output +=               '<div class="panel-body list-group list-group-contacts scroll" style="height: 200px;" id="each_notification">';
    //        if(reports.length > 0) {
    //            $.each(reports, function( key, value ) {
    //                output +=                   '<a class="list-group-item" href="/reports/show/'+value.hashId+'/read">';
    //                output +=                       '<img src="'+value.avatar+'" style="width:30px;" alt="John" class="pull-left img-circle">';
    //                output +=                       '<p>'+value.title+'</p>';
    //                output +=                       '<small class="text-muted">'+value.names+', '+value.date+' ('+value.ago+')</small>';
    //                output +=                    '</a>';
    //            });
    //        }
    //        output +=                '</div>';
    //        output +=           '</div>';
    //
    //        $('#notification_lists').html(output);
    //    },
    //    error: function(xhr,textStatus,error){
    //        alert(textStatus + ' ' + xhr);
    //    }
    //});

    ////////// PLEG Mobile User Socket ///////////////////////
    //var socket = io('http://plegapi.herokuapp.com');
    //
    //socket.on('connect', function(){
    //    alert('m connected!');
    //});

    //When new report record is created
    //socket.on('new report', function(id){
    //  $.ajax({
    //        type: 'GET',
    //            async: true,
    //            url: '/reports/new-report/11',// + id,
    //            success: function(data,textStatus){
    //            var output =    '<a class="list-group-item" href="/reports/show/'+data.hashId+'/read">';
    //            output +=           '<img src="'+data.avatar+'" style="width:30px;" alt="John" class="pull-left img-circle">';
    //            output +=           '<p>'+data.title+'</p>';
    //            output +=           '<small class="text-muted">'+data.names+', '+data.date+' ('+data.ago+')</small>';
    //            output +=        '</a>';
    //            $('#each_notification').prepend(output);
    //
    //            var count = parseInt($('#report_count').text()) + 1;
    //            $('#report_count').text(count);
    //            $('#report_count_unread').text(count);
    //
    //            noty({
    //                text: output,
    //                layout: 'topRight',
    //                type: 'alert',
    //                timeout: 6500,
    //                animation: {
    //                    open: {height: 'toggle'},
    //                    close: {height: 'toggle'},
    //                    easing: 'swing',
    //                    speed: 500,
    //                },
    //                dismissQueue: true, // If you want to use queue feature set this true
    //            });
    //        },
    //        error: function(xhr,textStatus,error){
    //            alert(textStatus + ' ' + xhr);
    //        }
    //    });
    //});
////////// PLEG Mobile User Socket ///////////////////////

    ////////Test new notification
    $(document.body).on('click', '#btn_test',function(e){
        $.ajax({
            type: 'GET',
            async: true,
            url: '/reports/new-report/5',// + id,
            success: function(data,textStatus){
                if(data) {
                    var output = '<a class="list-group-item" href="/reports/show/' + data.hashId + '/read">';
                    output += '<img src="' + data.avatar + '" style="width:30px;" alt="John" class="pull-left img-circle">';
                    output += '<p>' + data.title + '</p>';
                    output += '<small class="text-muted">' + data.names + ', ' + data.date + ' (' + data.ago + ')</small>';
                    output += '</a>';
                    $('#each_notification').prepend(output);

                    var count = parseInt($('#report_count').text()) + 1;
                    $('#report_count').text(count);
                    $('#report_count_unread').text(count);

                    noty({
                        text: output,
                        layout: 'topRight',
                        type: 'alert',
                        timeout: 6500,
                        animation: {
                            open: {height: 'toggle'},
                            close: {height: 'toggle'},
                            easing: 'swing',
                            speed: 500,
                        },
                        dismissQueue: true, // If you want to use queue feature set this true
                    });
                }
            },
            error: function(xhr,textStatus,error){
                alert(textStatus + ' ' + xhr);
            }
        });
    });
});