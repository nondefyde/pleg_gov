/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {

    //Button to Activate or Deactivate a User
    $(document.body).on('click', '.report_detail',function(e){
        $('#loader').addClass('show');
        $.ajax({
            type: 'GET',
            async: true,
            url: '/reports/detail/' + $(this).val(),
            success: function(data,textStatus){
                var output = '<div class="modal-header">';
                output += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                output += '<h4 class="modal-title" id="defModalHead">'+data.title+' in ('+data.state+' State)</h4></div>';
                output += '<div class="modal-body">\
                                <div class="row">\
                                <div class="col-xs-12 col-md-12">\
                                <h6>Report Information</h6>\
                            </div>';
                if(data.images.length > 0) {
                    for(var i=0; i<data.images.length; i++){
                        output += '<div class="col-xs-6 col-md-3">\
                                        <a href="#" class="thumbnail">\
                                            <img src="/'+data.path + data.images[i]+'">\
                                        </a>\
                                    </div>';
                    }
                    //$.each(data.images, function( key, value ) {
                    //    output += '<div class="col-xs-6 col-md-3">\
                    //                    <a href="#" class="thumbnail">\
                    //                        <img src="'+data.path +'/'+ value.image_url+'">\
                    //                    </a>\
                    //                </div>';
                    //});
                }
                if(data.description.length > 0) {
                    output += '<div class="col-xs-12 col-md-12">\
                                    <h6>Report Description</h6>\
                                </div>\
                                <div class="col-xs-12 col-md-12">\
                                    <p>'+data.description+'</p>\
                                </div>';
                }
                output += '<div class="col-xs-12 col-md-12">\
                                <div class="col-xs-6 col-md-6">\
                                    <strong><a href="#">23 people Backed this report</a></strong>\
                                </div>\
                                <div class="col-xs-6 col-md-6">\
                                    Reported By <img src="'+data.avatar+'" style="width:30px;" class="img-circle">\
                                    <a href="/mobile-users/activity/'+data.user_hashed+'">'+data.user.first_name+ ' ' +data.user.last_name+'</a>\
                                </div>\
                            </div>';
                output += '</div>\
                        </div>\
                    <div class="modal-footer">\
                        <a href="/reports/show/'+data.hashedId+'" class="btn btn-primary"><span class="fa fa-info"></span>More Details</a>\
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\
                    </div>';

                $('#modal_content').html(output);
                $('#loader').removeClass('show');
                $('#modal_basic').modal('show');
            },
            error: function(xhr,textStatus,error){
                //alert(textStatus + ' ' + xhr);
            }
        });
    });

    //Button to Forward a Report
    $(document.body).on('click', '.forward_report', function(e){
        e.preventDefault();
        $('#loader').addClass('show');
        $.ajax({
            type: 'GET',
            async: true,
            url: '/reports/forward-report/' + $(this).val(),
            success: function(data,textStatus){
                var output = '<div class="modal-header">';
                output += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                output += '<h4 class="modal-title" id="defModalHead">'+data.title+' in ('+data.state+' State)</h4></div>';
                output += '<div class="modal-body">\
                                <div class="row">\
                                    <div class="col-xs-12 col-md-12">\
                                        <h6>Forward to Organisation(s)</h6>\
                                    </div>';
                if(data.organisations.length > 0) {
                    output +=       '<div class="form-group">\
                                        <label class="col-md-3 col-xs-12 control-label">Select Organisation</label>\
                                        <div class="col-md-6 col-xs-12">\
                                            <select name="organisation_id[]" id="organisation_id" multiple class="form-control select" required>\
                                                <option value="">Nothing Selected</option>';
                    $.each(data.organisations, function (key, val) {
                        output += '<option value="' + val.organisation_id + '">' + val.name + '</option>';
                    });
                    output +=               '</select>\
                                            <input name="report_id" type="hidden" value="' + data.report_id + '">\
                                        </div>\
                                    </div>';

                    output +=   '</div>\
                            </div>\
                            <div class="modal-footer">\
                                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>\
                                <button type="submit" class="btn btn-primary pull-right"> <span class="fa fa-save"></span> Forward</button>\
                            </div>';
                }else{
                    output += '<div class="row">\
                                    <div class="col-xs-12 col-md-12">\
                                        <h4>No Organisation Registered Under this Sector Yet...</h4>\
                                    </div>\
                                    <div class="modal-footer">\
                                        <a href="/organisations/create/" class="btn btn-primary"><span class="fa fa-plus"></span>Add Organiation</a>\
                                    </div>\
                                </div>';
                }

                $('#forward_report_modal_content').html(output);
                $('#loader').removeClass('show');
                $('#forward_report_modal').modal('show');
            },
            error: function(xhr,textStatus,error){
                //alert(textStatus + ' ' + xhr);
            }
        });
    });
    //$(document.body).on('click', '.forward_report',function(e){
    //    e.preventDefault();
    //    var box = $("#confirm-forward-row");
    //    var parent = $(this).parent().parent();
    //
    //    $("#forward_name").text('Forward ' + parent.children(':nth-child(4)').text());
    //    $("#forward_status").val($(this).val());
    //    box.addClass("open");
    //});

    //Forwarding a Report
    //$(document.body).on('click', '#forward_status',function(e){
    //    e.preventDefault();
    //    var box = $("#confirm-forward-row");
    //    $.ajax({
    //        type: 'GET',
    //        async: true,
    //        url: '/reports/forward/' + $(this).val(),
    //        success: function(data,textStatus){
    //            box.removeClass("open");
    //            location.reload();
    //        },
    //        error: function(xhr,textStatus,error){
    //            alert(textStatus + ' ' + xhr);
    //        }
    //    });
    //    return false;
    //});

    //Button to Archive a Report
    $(document.body).on('click', '.archive_report',function(e){
        e.preventDefault();
        var box = $("#confirm-archive-row");
        var parent = $(this).parent().parent();

        $("#archive_name").text('Archive ' + parent.children(':nth-child(4)').text());
        $("#archive_status").val($(this).val());
        box.addClass("open");
    });

    //Archiving a Report
    $(document.body).on('click', '#archive_status',function(e){
        e.preventDefault();
        var box = $("#confirm-archive-row");
        $.ajax({
            type: 'GET',
            async: true,
            url: '/reports/archive/' + $(this).val(),
            success: function(data,textStatus){
                box.removeClass("open");
                location.reload();
            },
            error: function(xhr,textStatus,error){
                //alert(textStatus + ' ' + xhr);
            }
        });
        return false;
    });

    //Button to Approve a Report
    $(document.body).on('click', '.approve_report',function(e){
        e.preventDefault();
        var box = $("#confirm-approve-row");
        var parent = $(this).parent().parent();

        $("#approve_name").text('Approve ' + parent.children(':nth-child(4)').text());
        $("#approve_status").val($(this).val());
        box.addClass("open");
    });

    //Approving a Report
    $(document.body).on('click', '#approve_status',function(e){
        e.preventDefault();
        var box = $("#confirm-approve-row");
        $.ajax({
            type: 'GET',
            async: true,
            url: '/organisation-reports/approve/' + $(this).val(),
            success: function(data,textStatus){
                box.removeClass("open");
                location.reload();
            },
            error: function(xhr,textStatus,error){
                alert(textStatus + ' ' + xhr);
            }
        });
        return false;
    });

    //Button display the reported by mobile user information
    $(document.body).on('click', '.reported_by', function(e){
        $('#loader').addClass('show');
        $.ajax({
            type: 'GET',
            async: true,
            url: '/mobile-users/mobile-user/' + $(this).val(),
            success: function(data,textStatus){
                var output = '<div class="modal-header">';
                output +=           '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
                output +=           '<h4 class="modal-title" id="defModalHead">\
                                    <img src="'+data.avatar+'" style="width:30px" class="img-circle"/> '+data.first_name+' '+data.last_name+'</h4>\
                                </div>';
                output += '<div class="modal-body">\
                                <div class="row">\
                                    <div class="col-xs-6 col-md-4">\
                                        <div class="text-center" id="user_image">\
                                            <img src="'+data.avatar+'" class="img-thumbnail"/>\
                                        </div>\
                                    </div>';
                output += '<div class="col-xs-6 col-md-8">\
                                <table class="table table-bordered">\
                                    <tr>\
                                        <th>First Name</th>\
                                        <td>'+data.first_name+'</td>\
                                    </tr>\
                                    <tr>\
                                        <th>Last Name</th>\
                                        <td>'+data.last_name+'</td>\
                                    </tr>\
                                    <tr>\
                                        <th>Gender</th>\
                                        <td>'+data.gender+'</td>\
                                    </tr>\
                                    <tr>\
                                        <th>Email</th>\
                                        <td>'+data.email+'</td>\
                                    </tr>\
                                    <tr>\
                                        <th>Mobile</th>\
                                        <td>'+data.mobile+'</td>\
                                    </tr>\
                                    <tr>\
                                        <th>State</th>\
                                        <td>'+data.state+'</td>\
                                    </tr>\
                                    <tr>\
                                        <th>L.G.A</th>\
                                        <td>'+data.lga+'</td>\
                                    </tr>\
                                </table>\
                            </div>';

                output += '</div>\
                        </div>\
                    <div class="modal-footer">\
                        <a href="/mobile-users/activity/'+data.hashedId+'" class="btn btn-primary pull-left"> <span class="fa fa-clock-o"></span> Activity</a>\
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\
                    </div>';

                $('#reported_by_modal_content').html(output);
                $('#loader').removeClass('show');
                $('#reported_by_modal').modal('show');
            },
            error: function(xhr,textStatus,error){
                //alert(textStatus + ' ' + xhr);
            }
        });
    });
});