/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {

    //delete an organisation head
    $(document.body).on('click', '.delete_organisation',function(e){
        //e.preventDefault();
        var box = $("#confirm-remove-row");
        var parent = $(this).parent().parent();
        var organisation_name = parent.children(':nth-child(2)').text();

        $("#organisation_name").text(organisation_name);
        $("#confirm_organisation_delete").val($(this).val());
        box.addClass("open");
    });

    //confirm deleting of an organisation head
    $(document.body).on('click', '#confirm_organisation_delete',function(e){
        //e.preventDefault();
        var box = $("#confirm-remove-row");
        var sub_user_id = $(this).val();
        $.ajax({
            type: 'GET',
            async: true,
            url: '/organisation-heads/delete/' + sub_user_id,
            success: function(data,textStatus){
                box.removeClass("open");
                window.location.replace('/organisation-heads');
            },
            error: function(xhr,textStatus,error){
                alert(textStatus + ' ' + xhr);
            }
        });
        return false;
    });

    //Button to enable or disable an organisation head
    $(document.body).on('click', '.organisation_status',function(e){
        var box = $("#confirm-status-row");
        var parent = $(this).parent().parent();
        var status_type = ($(this).attr('rel') === '1') ? 'Enable' : 'Disable';

        $("#organisation_status").text(status_type + ' ' + parent.children(':nth-child(2)').text());
        $("#confirm_organisation_status").val($(this).val());
        $("#confirm_organisation_status").attr('rel', $(this).attr('rel'));
        box.addClass("open");
    });

    //Publishing or Un-publishing a project
    $(document.body).on('click', '#confirm_organisation_status',function(e){
        e.preventDefault();
        var box = $("#confirm-status-row");
        var user_id = $(this).val();
        var status = $(this).attr('rel');
        $.ajax({
            type: 'GET',
            async: true,
            url: '/organisation-heads/status/' + user_id + '/' + status,
            success: function(data,textStatus){
                box.removeClass("open");
                location.reload();
            },
            error: function(xhr,textStatus,error){
                alert(textStatus + ' ' + xhr);
            }
        });
        return false;
    });
});




